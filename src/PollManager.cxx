
#include <stdlib.h>
#include <sys/poll.h>
#include "transport/PollManager.h"

namespace daq {
    namespace transport {

        PollManager::PollManager()
            : m_num_ready(0),
              m_free_fd(0),
              m_poll(1024)
        {

            // initialize poll array
            for(size_t i = 0; i < m_poll.size(); i++) {
                m_poll[i].fd      = -1;
                m_poll[i].events  = 0;
                m_poll[i].revents = 0;
            }

            m_free_fd = m_poll.size();
        }

        PollManager::~PollManager() 
        {
        }

        void PollManager::add_handle(SOCKET s, Mode mode)
        {
            if(m_free_fd == 0) {

                size_t size = m_poll.size();

                m_poll.resize(m_poll.size() * 2);

                for(size_t i = size; i < 2 * size; i++) {
                    m_poll[i].fd      = -1;
                    m_poll[i].events  = 0;
                    m_poll[i].revents = 0;
                }

                m_free_fd = size;
            }

            // find a free slot
            for(size_t i = 0; i< m_poll.size(); i++) {
                if(m_poll[i].fd == -1) {
                    m_poll[i].fd      = s;
                    switch(mode) {
                    case Read:
                        m_poll[i].events  = POLLIN;
                        break;
                    case Write:
                        m_poll[i].events  = POLLOUT;                
                        break;
                    case ReadWrite:
                        m_poll[i].events  = POLLIN |POLLOUT;                
                        break;
                    }
                    m_poll[i].revents = 0;
                    m_free_fd--;
                    return;
                }
            }

            // should never happen, if there is no bug
            abort();
        }

        void PollManager::remove_handle(SOCKET s)
        {
            for(size_t i = 0; i < m_poll.size(); i++) {
                if(m_poll[i].fd == s) {
                    m_poll[i].fd      = -1;
                    m_poll[i].events  = 0;
                    m_poll[i].revents = 0;
                    m_free_fd++;
                    // this flushed the ready list
                    // this is to avoid that we ever return a removed
                    // SOCKET
                    m_num_ready = 0;
                    break;
                }
            }
        }

        // check if any socket is ready
        bool PollManager::ready()
        {
            if(m_num_ready == 0) {
                wait_poll(0);
            }

            return (m_num_ready != 0);
        }

        // waiting for data, returns SOCKET_ERROR if none available
        SOCKET PollManager::next()
        {
            if(m_num_ready == 0) {
                wait_poll(-1);
            }

            if(m_num_ready > 0) {
                return get_next();
            }

            return SOCKET_ERROR;
        }

        // timeout is in microseconds
        SOCKET PollManager::next(unsigned int timeout)
        {
            if(m_num_ready == 0) {
                wait_poll(timeout);
            }

            if(m_num_ready > 0) {
                return get_next();
            }

            return SOCKET_ERROR;    
        }

        void PollManager::wait_poll(int timeout)
        {
            int n = poll(&m_poll[0], m_poll.size(), (timeout != -1) ? timeout/1000 : -1);
            if(n >= 0) {
                m_num_ready = n;
            } else /* if (n < 0) */ {
                abort();
                // what to do in case of error ??
            }
        }

        SOCKET PollManager::get_next()
        {

            for(size_t i = 0; i < m_poll.size(); i++) {
                if((m_poll[i].fd != -1) && (m_poll[i].revents & (POLLIN|POLLOUT))) {
                    // got one...
                    m_poll[i].revents = 0;
                    m_num_ready--;
                    return m_poll[i].fd;
                }
            }

            return SOCKET_ERROR;
        }
    }
}
