//
// $Id$
//

#include "transport/SocketDefs.h"

#include <string.h>
#include "transport/NetAddress.h"
#include <iostream>

using namespace std;

// the following prototype is missing on some systems
// like Solaris 2.5 ?
// note that Digital Unix has two version, one with
// int and one with size_t...
// extern "C"
// int gethostname(char *name, size_t size);

// constructor
NetAddress::NetAddress(const std::string& hostname, daq::transport::IPPort port)
    : m_resolved(false),
      m_hostname(hostname), 
      m_port(port)
{
    memset(&m_addr, 0, sizeof(m_addr));
}

// Create NetAddress from IP address
NetAddress::NetAddress(daq::transport::IPAddress addr, daq::transport::IPPort port)
    : m_resolved(false),
      m_hostname(),
      m_port(port)
{
    memset(&m_addr, 0, sizeof(m_addr));

    ((sockaddr_in *)&m_addr)->sin_family = AF_INET;
    ((sockaddr_in *)&m_addr)->sin_addr.s_addr = htonl(addr);
}

// Create NetAddress from sockaddr_storage
NetAddress::NetAddress(const sockaddr_storage& addr)
    : m_resolved(true),
      m_hostname(),
      m_addr(addr)
{
    if(addr.ss_family == AF_INET) {
        sockaddr_in *p = (sockaddr_in* )&m_addr;
        m_port = ntohs(p->sin_port);
    } else if (addr.ss_family == AF_INET6) {
        sockaddr_in6 *p = (sockaddr_in6* )&m_addr;
        m_port = ntohs(p->sin6_port);        
    }
}



// copy constructor
NetAddress::NetAddress(const NetAddress& other)
    : m_resolved(other.m_resolved),
      m_hostname(other.m_hostname), 
      m_port(other.m_port),
      m_addr(other.m_addr)
{
}

// destructor, nothing to do
NetAddress::~NetAddress()
{
}

// assignment
NetAddress& NetAddress::operator=(const NetAddress& other)
{
    m_resolved  = other.m_resolved;
    m_hostname  = other.m_hostname;
    m_port      = other.m_port;
    m_addr      = other.m_addr;
    return *this;
}

int  NetAddress::family() const
{
    if(resolve()) {
        return m_addr.ss_family;
    }
    return AF_UNSPEC;
}

// get port number
daq::transport::IPPort NetAddress::port() const
{
    return m_port;
}

// get hostname
std::string NetAddress::hostname() const
{
    if(m_hostname.empty()) {
        char buffer[1024];
        if(getnameinfo((sockaddr *)&m_addr, sizeof(m_addr), buffer ,sizeof(buffer), 0, 0, 0) == 0) {
            m_hostname = buffer;
        }
    }

    return m_hostname;
}

// get IP address, resolve it if necessary
daq::transport::IPAddress NetAddress::inet_addr() const
{
    if(resolve()) {
        if(m_addr.ss_family == AF_INET) {
            return ntohl(((sockaddr_in *)&m_addr)->sin_addr.s_addr);
        }
    }

    return 0;
}

in6_addr NetAddress::inet_addr_v6() const
{
    if(resolve()) {
        if(m_addr.ss_family == AF_INET6) {
            return ((sockaddr_in6 *)&m_addr)->sin6_addr;
        }
    }

    return in6addr_any;
}

bool NetAddress::getsockaddr(sockaddr_storage& saddr) const
{
    if(resolve()) {
        saddr = m_addr;
        return true;
    } else {
        return false;
    }
}

bool NetAddress::resolve() const
{
    if(!m_resolved) {

        addrinfo *results;
        addrinfo hints = { AI_ADDRCONFIG, AF_UNSPEC, 0, 0, 0, 0, 0, 0 };
        
        if(getaddrinfo(m_hostname.c_str(), 
                       0,
                       &hints,
                       &results) == 0) {
            if(results) {
                memcpy(&m_addr, results->ai_addr, results->ai_addrlen);
                ((sockaddr_in *)&m_addr)->sin_port = htons(m_port);
                freeaddrinfo(results);
                return true;
            }
        }
        return false;
    }
    return true;
}

// return our own host name
std::string NetAddress::self() 
{
    char buf[256];
    
    if(gethostname(buf,sizeof(buf)) == 0) {
        return std::string(buf);
    } else {
        return std::string("UnknownHostname");
    }
}

std::ostream& operator<<(std::ostream& os, const NetAddress& naddr)
{
    os << naddr.hostname() << ' ';
    if(naddr.resolve()) {
        char buf[128];
        if(getnameinfo((const sockaddr*)&naddr.m_addr, sizeof(naddr.m_addr),
                       buf, sizeof(buf), 0, 0, NI_NUMERICHOST) == 0) {
            os << buf;
        }
        os << " : " << naddr.m_port;
    }
    return os;
}
