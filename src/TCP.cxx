//
// $Id$
//

#include <signal.h>
#include <fcntl.h>
#include <cassert>
#include <cstdio>
#include <errno.h>
#include <string.h>
#include <poll.h>

#include "transport/SocketDefs.h"
#include "transport/NetAddress.h"
#include "transport/TCP.h"

using namespace transport;

//
// helper class to make sure we ignore the SIGPIPE signal
// and always get a EPIPE error instead
//
class SIGPIPEHandler {
public:
    SIGPIPEHandler();
};

SIGPIPEHandler::SIGPIPEHandler()
{
    ::signal(SIGPIPE, SIG_IGN);
}

//
// declare a static variable here, so every use of this
// libary will install the handler.
//
static SIGPIPEHandler s_handler;

void TCP::common_init(int af)
{
    char service[32];
    std::sprintf(service, "%d", m_port);
    addrinfo hints = { AI_PASSIVE | AI_ADDRCONFIG, af, SOCK_STREAM, IPPROTO_TCP, 0, 0, 0, 0 };
    addrinfo *result = 0;

    int err = getaddrinfo(0, service, &hints, &result);

    if(err != 0) {
        throw TransportCannotCreate(ERS_HERE, gai_strerror(err));
    }

    if(result == 0) {
        throw TransportCannotCreate(ERS_HERE, "Invalid arguments");
    }

    m_socket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);

    if(m_socket == INVALID_SOCKET) {
        freeaddrinfo(result);
        throw TransportCannotCreate(ERS_HERE, strerror(errno));
    }

    int no = 0;
    setsockopt (m_socket, SOL_IPV6, IPV6_V6ONLY, &no, sizeof(no));
    
    // if local_port != 0 bind socket
    if(m_port != 0) {
        
        // first set socket option to reuse address
        int option = 1;
        setsockopt(m_socket, SOL_SOCKET, SO_REUSEADDR,
                   (char *)&option, sizeof(option));

        if(bind(m_socket,
                result->ai_addr,
                result->ai_addrlen) == SOCKET_ERROR){
	    closesocket(m_socket);
            freeaddrinfo(result);
            throw TransportCannotBind(ERS_HERE, strerror(errno), m_port);
        }
    }

    freeaddrinfo(result);
    
}

TCP::TCP(const NetAddress& addr, unsigned short local_port)
    : Transport(addr),  
      m_port(local_port), 
      m_socket(INVALID_SOCKET),
      m_listening(false),
      m_is_nonblocking(false)
{


    // connect socket to 'addr' with no timeout
    const NetAddress& laddr = address();
    
    struct sockaddr_storage saddr;
    laddr.getsockaddr(saddr);

    common_init(saddr.ss_family);
    
    if(connect(m_socket, 
               (struct sockaddr *)&saddr, 
               sizeof(saddr)) == SOCKET_ERROR) {
	closesocket(m_socket);
        throw TransportCannotConnect(ERS_HERE, strerror(errno), addr.hostname(), addr.port());
    }
}

TCP::TCP(const NetAddress& addr, unsigned short local_port, unsigned int timeout_in_us)
   : Transport(addr),  
      m_port(local_port), 
      m_socket(INVALID_SOCKET),
      m_listening(false),
      m_is_nonblocking(false)
{
    const NetAddress& laddr = address();
    
    struct sockaddr_storage saddr;
    laddr.getsockaddr(saddr);

    common_init(saddr.ss_family);
    
    set_nonblocking(true);
    int err = connect(m_socket, 
                      (struct sockaddr *)&saddr, 
                      sizeof(saddr));

    // Success
    if(err == 0) { 
        set_nonblocking(false);
        return;
    }

    // An error occured
    if(err == SOCKET_ERROR && errno != EINPROGRESS) {
	closesocket(m_socket);
        throw TransportCannotConnect(ERS_HERE, strerror(errno), addr.hostname(), addr.port());
    }

    // Else wait for 'timeout_in_us' microseconds
    fd_set wr_set;
    FD_ZERO(&wr_set);
    FD_SET(m_socket, &wr_set);

    timeval timeout = { timeout_in_us/1000000, (timeout_in_us % 1000000) };
    int n = select(m_socket + 1, 0, &wr_set, 0, &timeout);
    if(n == 0) {
        closesocket(m_socket);
        errno = ETIMEDOUT;
        throw TransportCannotConnect(ERS_HERE, strerror(errno), addr.hostname(), addr.port());
    } else if (n == 1) {
        int err;
        socklen_t len = sizeof(err);
        getsockopt(m_socket, SOL_SOCKET, SO_ERROR, &err, &len);
        if(err != 0) {
            closesocket(m_socket);
            errno = err;
            throw TransportCannotConnect(ERS_HERE, strerror(errno), addr.hostname(), addr.port());
        }
    }
    set_nonblocking(false);
}

TCP::TCP(unsigned short local_port)
    : Transport(NetAddress("",0)),  
      m_port(local_port), 
      m_listening(true),
      m_is_nonblocking(false)
{
    common_init(AF_UNSPEC);

    if(listen(m_socket, SOMAXCONN) == SOCKET_ERROR) {
	closesocket(m_socket);
        throw TransportCannotCreate(ERS_HERE, strerror(errno));
    }
}

TCP::TCP(SOCKET socket, unsigned short local_port, const NetAddress& addr)
    : Transport(addr),
      m_port(local_port),
      m_socket(socket),
      m_listening(false),
      m_is_nonblocking(false)
{
}      


TCP::~TCP()
{
    // ignore errors
    closesocket(m_socket);
}

size_t TCP::receive_count(void *data, size_t length)
{
    char *ptr = (char *)data;
    
    int n = recv(m_socket, ptr, length, MSG_WAITALL);
    if(n < 0) {
        // error 
        throw transport::TransportCannotReceive(ERS_HERE, strerror(errno));
    }
    // may still be less than expected, e.g. at end of file
    return (size_t )n;
}

size_t TCP::receive_count(void *data, size_t length, unsigned int timeout)
{
    char *ptr = (char *)data;

    // set timeout
    timeval to = { timeout/1000000, (timeout % 1000000) };
    setsockopt(m_socket, SOL_SOCKET, SO_RCVTIMEO, &to, sizeof(to));

    int n = recv(m_socket, ptr, length, MSG_WAITALL);
    int err = errno;

    // reset timeout
    to.tv_sec  = 0;
    to.tv_usec = 0;
    setsockopt(m_socket, SOL_SOCKET, SO_RCVTIMEO, &to, sizeof(to));

    if(n < 0) {
        if( err != EAGAIN) {
            // error 
            throw transport::TransportCannotReceive(ERS_HERE, strerror(err));
        } else {
            return 0;
        }
    }
    return (size_t )n;
}

bool TCP::send(int n, struct iovec *vec)
{
    while (n > UIO_MAXIOV) {
        struct msghdr hdr = { 0, 0, vec, UIO_MAXIOV, 0, 0, 0 };
        if(::sendmsg(m_socket, &hdr, MSG_NOSIGNAL) == SOCKET_ERROR) return false;
        n -= UIO_MAXIOV;
        vec += UIO_MAXIOV;
    }
    struct msghdr hdr = { 0, 0, vec, (size_t)n, 0, 0, 0 };
    return ::sendmsg(m_socket, &hdr, MSG_NOSIGNAL) != SOCKET_ERROR;        
}

TCP *TCP::accept()
{
    if(!m_listening) {
        return 0;
    }

    struct sockaddr_storage addr;
    socklen_t          addr_len = sizeof(addr);

    SOCKET new_socket = ::accept(m_socket, (struct sockaddr *)&addr, &addr_len);

    if(new_socket == INVALID_SOCKET) {
        return 0;
    }

    TCP *result = new TCP(new_socket, m_port, 
                          NetAddress(addr));

    return result;
}

bool TCP::set_nodelay(bool on)
{
    int option = on;
    return (setsockopt(m_socket, SOL_TCP, TCP_NODELAY, &option, sizeof(option)) != SOCKET_ERROR);
}


bool TCP::set_nonblocking(bool on)
{
    int flags = fcntl(m_socket, F_GETFL);
    if(on) {
        flags |= O_NONBLOCK;
    } else {
        flags &= ~O_NONBLOCK;
    }
    return (fcntl(m_socket, F_SETFL, flags) != -1);
}

bool TCP::is_nonblocking() const
{
    int flags = fcntl(m_socket, F_GETFL);
    return flags & O_NONBLOCK; 
}


SOCKET TCP::get_handle() const
{
    return m_socket;
}
