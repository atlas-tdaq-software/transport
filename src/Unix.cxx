
#include "transport/Unix.h"

#include <errno.h>
#include <poll.h>
#include "transport/SocketDefs.h"
#include "transport/NetAddress.h"

#define UNIX_PATH_MAX 108

namespace daq {

    namespace transport {

        Unix::Unix(const std::string& path, bool server)
            : Transport(NetAddress(path, 0)),
              m_socket(INVALID_SOCKET),
              m_server(server)
        {
            m_socket = socket(PF_LOCAL, SOCK_STREAM, 0);
            if(m_socket == INVALID_SOCKET) {
                throw TransportCannotCreate(ERS_HERE, strerror(errno));
            }

            struct sockaddr_un addr;
            memset(&addr, 0, sizeof(addr));
            addr.sun_family = AF_UNIX;
            strncpy(&addr.sun_path[0], path.c_str(), UNIX_PATH_MAX-1);

            if(m_server) {
                if(bind(m_socket, 
                        (struct sockaddr *)&addr,
                        sizeof(addr)) == SOCKET_ERROR) {
                    closesocket(m_socket);
                    throw TransportCannotBind(ERS_HERE, strerror(errno), 0);
                }

                if(listen(m_socket, SOMAXCONN) == SOCKET_ERROR) {
                    closesocket(m_socket);
                    throw TransportCannotCreate(ERS_HERE, strerror(errno));
                }
            } else {
                if(connect(m_socket, 
                           (struct sockaddr *)&addr, 
                           sizeof(addr)) == SOCKET_ERROR) {
                    closesocket(m_socket);
                    throw TransportCannotConnect(ERS_HERE, strerror(errno), path, 0);
                }
            }
        }

        Unix::Unix(SOCKET new_socket)
            : Transport(NetAddress("", 0)),
              m_socket(new_socket),
              m_server(false)
        {
        }

        Unix::~Unix()
        {
            closesocket(m_socket);
        }

        Unix *Unix::accept()
        {
            if(!m_server) { 
                return 0;
            }

            SOCKET new_socket = ::accept(m_socket, 0, 0);
            if(new_socket == INVALID_SOCKET) {
                return 0;
            }

            return new Unix(new_socket);
        }

        // get underlying SOCKET
        SOCKET Unix::get_handle() const
        {
            return m_socket;
        }
        
        // return local port number
        unsigned short Unix::local_port()
        {
            return 0;
        }
        
    }
    
}

