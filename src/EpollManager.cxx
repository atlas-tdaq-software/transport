
#include "transport/EpollManager.h"

#include "transport/Transport.h"

#include <stdlib.h>
#include <sys/epoll.h>
#include <errno.h>


namespace daq {
    namespace transport {

        EpollManager::EpollManager(int size)
            : m_poll(0),
              m_max_fd(size),
              m_num_ready(0),
              m_fd(INVALID_SOCKET)
        {
            m_fd    = epoll_create(size);
            if(m_fd < 0) {
                throw ::transport::TransportNotSupported(ERS_HERE, strerror(errno));
                return;
            }
            // initialize poll array
            m_poll  = new struct epoll_event[size];

        }

        EpollManager::~EpollManager() 
        {
            // delete the allocated data structures
            delete [] m_poll;
            close(m_fd);
        }

        void EpollManager::add_handle(SOCKET s, Mode mode)
        {

            struct epoll_event ev;
            switch(mode) {
            case Read:
                ev.events  = EPOLLIN;
                break;
            case Write:
                ev.events  = EPOLLOUT;
                break;
            case ReadWrite:
                ev.events  = EPOLLIN | EPOLLOUT;
                break;
            }

            ev.data.fd = s;

            if(epoll_ctl(m_fd, EPOLL_CTL_ADD, s, &ev)  < 0){
                throw ::transport::TransportNotSupported(ERS_HERE, strerror(errno));
            }
        }

        void EpollManager::remove_handle(SOCKET s)
        {
    
            if(epoll_ctl(m_fd, EPOLL_CTL_DEL, s, 0) < 0) {
                // Do not throw if an invalid socket is given.
                //
                // Not all call sites may ensure that they never
                // call this routine twice.
                // 
                // throw ::transport::TransportNotSupported(ERS_HERE, strerror(errno));
            }

            // "flush" the list of ready descriptors
            m_num_ready = 0;
        }

        // check if any socket is ready
        bool EpollManager::ready()
        {
            if(m_num_ready == 0) {
                wait_poll(0);
            }

            return (m_num_ready != 0);
        }

        // waiting for data, returns SOCKET_ERROR if none available
        SOCKET EpollManager::next()
        {
            if(m_num_ready == 0) {
                wait_poll(-1);
            }

            if(m_num_ready > 0) {
                return get_next();
            }

            return SOCKET_ERROR;
        }

        // timeout is in microseconds
        SOCKET EpollManager::next(unsigned int timeout)
        {
            if(m_num_ready == 0) {
                wait_poll(timeout);
            }

            if(m_num_ready > 0) {
                return get_next();
            }

            return SOCKET_ERROR;    
        }

        void EpollManager::wait_poll(int timeout)
        {
            int n = epoll_wait(m_fd, m_poll, m_max_fd, timeout != -1 ? timeout/1000 : -1);
            if(n >= 0) {
                m_num_ready = n;
            } else if (n < 0) {
                // what to do in case of error ??
            }
        }

        SOCKET EpollManager::get_next()
        {
            if(m_num_ready > 0) {
                m_num_ready--;
                return m_poll[m_num_ready].data.fd;
            }

            return SOCKET_ERROR;
        }
    } // namespace transport
} // namespace daq
