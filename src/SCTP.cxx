//
// $Id$
//

#include <signal.h>
#include <fcntl.h>
#include <cstdio>
#include <errno.h>
#include <string.h>
#include <poll.h>

#include "transport/SocketDefs.h"
#include "transport/NetAddress.h"
#include "transport/SCTP.h"

#include <netinet/sctp.h>

#ifndef HAVE_SCTP_PRSCTP
// older header file
#define SCTP_UNORDERED MSG_UNORDERED
#endif

using namespace transport;

SCTP::SCTP(const NetAddress& addr, unsigned short local_port)
    : Transport(addr),  
      m_port(local_port), 
      m_socket(INVALID_SOCKET),
      m_listening(false)
{
    const NetAddress& laddr = address();
    
    struct sockaddr_storage saddr;
    laddr.getsockaddr(saddr);

    char service[32];
    std::sprintf(service, "%d", m_port);
    addrinfo hints = { AI_PASSIVE | AI_ADDRCONFIG, saddr.ss_family, SOCK_STREAM, 0, 0, 0, 0, 0 };
    addrinfo *result = 0;

    int err = getaddrinfo(0, service, &hints, &result);
    if(err != 0) {
        throw TransportCannotCreate(ERS_HERE, gai_strerror(err));
    }

    if(result == 0) {
        throw TransportCannotCreate(ERS_HERE, "Invalid arguments");
    }

    m_socket = socket(result->ai_family, result->ai_socktype, IPPROTO_SCTP);

    if(m_socket == INVALID_SOCKET) {
        throw TransportCannotCreate(ERS_HERE, strerror(errno));
    }

    int no = 0;
    setsockopt (m_socket, SOL_IPV6, IPV6_V6ONLY, &no, sizeof(no));

    // if local_port != 0 bind socket
    if(m_port != 0) {
        
        // first set socket option to reuse address
        int option = 1;
        setsockopt(m_socket, SOL_SOCKET, SO_REUSEADDR,
                   (char *)&option, sizeof(option));


        if(bind(m_socket,
                result->ai_addr,
                result->ai_addrlen) == SOCKET_ERROR){
	    closesocket(m_socket);
            throw TransportCannotBind(ERS_HERE, strerror(errno), m_port);
        }
    }
    
    // connect socket to 'addr'
    if(connect(m_socket, 
               (struct sockaddr *)&saddr, 
               sizeof(saddr)) == SOCKET_ERROR) {
	closesocket(m_socket);
        throw TransportCannotConnect(ERS_HERE, strerror(errno), addr.hostname(), addr.port());
    }
}

SCTP::SCTP(unsigned short local_port, bool stream, int num_streams)
    : Transport(NetAddress("",0)),  
      m_port(local_port), 
      m_listening(stream),
      m_num_streams(num_streams)
{
    // create socket
    m_socket = socket(PF_INET6, stream ? SOCK_STREAM : SOCK_SEQPACKET, IPPROTO_SCTP);

    if(m_socket == INVALID_SOCKET) {
        throw TransportCannotCreate(ERS_HERE, strerror(errno));
    }

    int no = 0;
    setsockopt (m_socket, SOL_IPV6, IPV6_V6ONLY, &no, sizeof(no));

    if(num_streams > 0) {
        sctp_initmsg initmsg = { (uint16_t )num_streams, (uint16_t)num_streams, 0, 0 };
        setsockopt(m_socket, SOL_SCTP, SCTP_INITMSG, &initmsg, sizeof(initmsg));
    }

    int option = 1;
    setsockopt(m_socket, SOL_SOCKET, SO_REUSEADDR,
               (char *)&option, sizeof(option));

    // if local_port != 0 bind socket
    if(m_port != 0) {
        struct sockaddr_in6 saddr;
        memset(&saddr, 0, sizeof(saddr));

        saddr.sin6_family = AF_INET6;
        saddr.sin6_port   = htons(m_port);
        saddr.sin6_addr   = in6addr_any;

        if(bind(m_socket, 
                (struct sockaddr *)&saddr,
                sizeof(saddr)) == SOCKET_ERROR){
	    closesocket(m_socket);
            throw TransportCannotBind(ERS_HERE, strerror(errno), m_port);
        }
    }

    if(listen(m_socket, SOMAXCONN) == SOCKET_ERROR) {
	closesocket(m_socket);
        throw TransportCannotCreate(ERS_HERE, strerror(errno));
    }
}

SCTP::SCTP(SOCKET socket, unsigned short local_port, const NetAddress& addr)
    : Transport(addr),
      m_port(local_port),
      m_socket(socket),
      m_listening(false)
{
}      


SCTP::~SCTP()
{
    // ignore errors
    closesocket(m_socket);
}

bool SCTP::send(int n, struct iovec *vec, const NetAddress& addr, int stream, bool unordered)
{
    struct sockaddr_storage saddr;

    addr.getsockaddr(saddr);

    struct msghdr hdr = { &saddr, sizeof(saddr), vec, (size_t)n, 0, 0, 0 };

    if(stream > 0 || unordered) {
        // need a CMSG entry

        // clear structure
        sctp_sndrcvinfo sndrcv;
        memset(&sndrcv, 0, sizeof(sndrcv));

        // set only stream and flags
        sndrcv.sinfo_stream = stream;
        if(unordered) {
            sndrcv.sinfo_flags  |= SCTP_UNORDERED;
        }

        // CMSG acrobatics
        
        char buf[CMSG_SPACE(sizeof sndrcv)];

        hdr.msg_control = buf;
        hdr.msg_controllen = sizeof(buf);

        struct cmsghdr *cmsg = CMSG_FIRSTHDR(&hdr);
        cmsg->cmsg_level = SOL_SCTP;
        cmsg->cmsg_type  = SCTP_SNDRCV;
        cmsg->cmsg_len   = CMSG_LEN(sizeof(sndrcv));
        memcpy(CMSG_DATA(cmsg), &sndrcv, sizeof(sndrcv));

        hdr.msg_controllen = cmsg->cmsg_len;
    }

    return ::sendmsg(m_socket, &hdr, MSG_NOSIGNAL) != SOCKET_ERROR;
}

SCTP *SCTP::accept()
{
    if(!m_listening) {
        return 0;
    }

    struct sockaddr_storage addr;
    socklen_t          addr_len = sizeof(addr);

    SOCKET new_socket = ::accept(m_socket, (struct sockaddr *)&addr, &addr_len);

    if(new_socket == INVALID_SOCKET) {
        return 0;
    }

    SCTP *result = new SCTP(new_socket, m_port, 
			    NetAddress(addr));

    return result;
}

SOCKET SCTP::get_handle() const
{
    return m_socket;
}

size_t SCTP::receive(int n, iovec *vec, bool& more)
{
    struct msghdr hdr = { 0, 0, vec, (size_t)n, 0, 0, 0 };
    int res = ::recvmsg(get_handle(), &hdr, 0);
    if(res < 0) {
        throw TransportCannotReceive(ERS_HERE, strerror(errno));
    }
    more = ((hdr.msg_flags & MSG_EOR) == 0);
    return (size_t )res;    
}

bool SCTP::set_nodelay(bool on)
{
    int option = on;
    return (setsockopt(m_socket, SOL_SCTP, SCTP_NODELAY, &option, sizeof(option)) != SOCKET_ERROR);
}

bool SCTP::set_oobinline(bool on)
{
    int option = on;
    return (setsockopt(m_socket, SOL_SOCKET, SO_OOBINLINE, &option, sizeof(option)) != SOCKET_ERROR);    
}

int SCTP::num_streams() const
{
    return m_num_streams;
}
