
#include "transport/SocketDefs.h"
#include "transport/SelectManager.h"

#include <cassert>
#include <cstring>

namespace daq {
    namespace transport {

        SelectManager::SelectManager() 
            : m_max(0),
              m_num_ready(0)
      
        {
            FD_ZERO(&m_set);
            FD_ZERO(&m_write_set);
            FD_ZERO(&m_ready);
            FD_ZERO(&m_write_ready);
        }

        void SelectManager::add_handle(SOCKET s, Mode mode) 
        {
            assert(s < FD_SETSIZE);
            
            switch(mode) {
            case Read:
                FD_SET(s, &m_set);
                break;
            case Write:
                FD_SET(s, &m_write_set);                
                break;
            case ReadWrite:
                FD_SET(s, &m_set);                
                FD_SET(s, &m_write_set);                
                break;
            }

            if(s >= m_max) {
                m_max = s + 1;
            }
    
            // clear the cached information
            m_num_ready = 0;
            FD_ZERO(&m_ready);
            FD_ZERO(&m_write_ready);
        }

        void SelectManager::remove_handle(SOCKET s)
        {
            FD_CLR(s, &m_set);
            FD_CLR(s, &m_write_set);

#if 0    
            // the following might not make a big difference,
            // the kernel would only check a couple more bits
            // in the fd_set than necessary...
    
            if(s == m_max + 1) {    // in case this is the highest socket
                for(SOCKET i = 0; i < m_max; i++) {
                    if(FD_ISSET(i, &m_set)) {
                        m_max = i + 1;
                    }
                }
            } // m_max is now the highest socket fd + 1
#endif    
            // clear the cached information
            m_num_ready = 0;
            FD_ZERO(&m_ready);
            FD_ZERO(&m_write_ready);
        }

        bool SelectManager::ready()
        {
            if(m_num_ready == 0) {
                wait_more(0);
            }
            return (m_num_ready != 0);
        }


        SOCKET SelectManager::next()
        {
            if(m_num_ready > 0) {
                return get_next();
            }
            wait_more(0);
            return get_next();
        }

        SOCKET SelectManager::next(unsigned int timeout)
        {
            if(m_num_ready > 0) {
                return get_next();
            }
            struct timeval tv = { timeout/1000000, timeout % 1000000 };
            wait_more(&tv);
            return get_next();
        }

        void SelectManager::wait_more(struct timeval *timeout)
        {
            // do a new select()
    
            // fd_set is a struct in Linux, but it may also
            // be an array in other implementations ??
            // so just to be sure
    
            // if there is only a small number of sockets,
            // it might be faster to just loop over them
            // and set the appropriate bits in 'set'
    
            memcpy(&m_ready, &m_set, sizeof(m_set));
    
            // block forever
            int n = select(m_max, &m_ready, 0, 0, timeout);
            if(n < 0) {
                return;
            }
            m_num_ready = n;
        }

        SOCKET SelectManager::get_next()
        {
            for(SOCKET i = 0; i < m_max; i++) {
                if(FD_ISSET(i, &m_ready)) {
                    FD_CLR(i,&m_ready);
                    m_num_ready--;
                    return i;
                }
                if(FD_ISSET(i, &m_write_ready)) {
                    FD_CLR(i,&m_write_ready);
                    m_num_ready--;
                    return i;
                }
            }
            return SOCKET_ERROR;
        }
    }
}
