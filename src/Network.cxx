
#include "transport/Network.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <iostream>

#include <boost/lexical_cast.hpp>

namespace daq {
    namespace transport {
        
        Network::Network(const std::string& arg)
            : m_addr(0),
              m_mask(0)
        {
            using namespace std;
            string nw(arg);
            string::size_type i = nw.find('/');
            if(i != string::npos) {
                string mask = nw.substr(i+1);
                nw = nw.substr(0, i);
                i = mask.find('.');
                if(i != string::npos) {
                    in_addr addr;
                    if(inet_pton(AF_INET, mask.c_str(), &addr) <= 0) {
                        throw ::transport::NetworkInvalidAddress(ERS_HERE);
                    }
                    m_mask = ntohl(addr.s_addr);
                } else {
                    unsigned int bits = boost::lexical_cast<unsigned int>(mask);
                    if(bits > 31) {
                        throw ::transport::NetworkInvalidAddress(ERS_HERE);
                    }
                    IPAddress result = 1;
                    for(unsigned x = 0; x < (31 - bits); x++) {
                        result <<= 1;
                        result |= 1;
                    }
                    result = ~result;
                    m_mask = result;
                }
            }

            in_addr addr;
            if(inet_pton(AF_INET, nw.c_str(), &addr) <= 0) {
                throw ::transport::NetworkInvalidAddress(ERS_HERE);
            }
            m_addr = ntohl(addr.s_addr);

            if(m_mask == 0) {
                if(IN_CLASSA(m_addr)) {
                    m_mask = IN_CLASSA_NET;
                } else if(IN_CLASSB(m_addr)) {
                    m_mask = IN_CLASSB_NET;
                } else if(IN_CLASSC(m_addr)) {
                    m_mask = IN_CLASSC_NET;
                }
            }
        }

        Network::Network(IPAddress addr, IPAddress mask)
            : m_addr(addr),
              m_mask(mask)
        {
        }

        IPAddress Network::address() const
        {
            return m_addr;
        }

        IPAddress Network::netmask() const
        {
            return m_mask;
        }
        
        bool Network::operator==(const Network& other) const
        {
            return m_addr == other.m_addr && m_mask == other.m_mask;
        }

        bool Network::matches(IPAddress addr) const
        {
            return (addr & m_mask) == m_addr;
        }

        std::ostream& operator<<(std::ostream& os, const Network& nw)
        {
            in_addr addr = { htonl(nw.address()) };
            in_addr mask = { htonl(nw.netmask()) };
            char buf[64];
            char buf1[64];

            os << "ADDR: " << inet_ntop(AF_INET, &addr, buf, sizeof(buf)) << " MASK: " << inet_ntop(AF_INET, &mask, buf1, sizeof(buf1));
            return os;
        }

    }
}

