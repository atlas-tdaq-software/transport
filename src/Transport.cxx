
//
// $Id$
//


#include "transport/Transport.h"

#include <stdlib.h>
#include <list>
#include <utility>
#include <sstream>
#include <cassert>
#include <errno.h>
#include <poll.h>

using namespace std;
using namespace transport;

// constructor, nothing yet
Transport::Transport(const NetAddress& addr)
    : m_addr(addr)
{
}

// destructor, nothing to do
Transport::~Transport()
{
}

bool Transport::send(const void *data, size_t length)
{
    struct iovec vec[1];
    vec[0].iov_base = (void *)data;
    vec[0].iov_len  = length;

    return send(1, vec);
}

// default gather send
bool Transport::send(std::list<std::pair<void*,size_t> >& data)
{
    struct iovec vec[UIO_MAXIOV];

    assert(data.size() <= UIO_MAXIOV);

    int n = 0;
    for(std::list<std::pair<void *,size_t> >::iterator it = data.begin();
        it != data.end();
        it++) {
        vec[n].iov_base = it->first;
        vec[n].iov_len  = it->second;
        ++n;
    }
    return send(n, vec);
}

bool Transport::send(int n, struct iovec *vec)
{
    struct msghdr hdr = { 0, 0, vec, (size_t )n, 0, 0, 0 };
    return ::sendmsg(get_handle(), &hdr, MSG_NOSIGNAL) != SOCKET_ERROR;    
}


// default scatter receive
size_t Transport::receive(std::list<std::pair<void *, size_t> >& data)
{

    assert(data.size() <= UIO_MAXIOV);
    struct iovec vec[UIO_MAXIOV];

    int i = 0;
    for(std::list<std::pair<void*,size_t> >::iterator it = data.begin();
        it != data.end();
        ++it) {
        vec[i].iov_base = it->first;
        vec[i].iov_len  = it->second;
        i++;
    }

    return receive(i, vec);
}

size_t Transport::receive(int n, iovec *vec)
{
    struct msghdr hdr = { 0, 0, vec, (size_t )n, 0, 0, 0 };
    int res = ::recvmsg(get_handle(), &hdr, 0);
    if(res < 0) {
        throw TransportCannotReceive(ERS_HERE, strerror(errno));
    }

    return (size_t )res;    
}

size_t Transport::receive(void *data, size_t length, unsigned int timeout)
{

    SOCKET sock = get_handle();

    if(timeout != 0) {

	// in milliseconds for poll()
	timeout /= 1000;
 
	pollfd fd = { 
            sock,
	    POLLIN,
	    0
	};

        // poll with timeout
	int n = poll(&fd, 1, timeout);
        if(n == SOCKET_ERROR) {
            throw TransportCannotReceive(ERS_HERE, strerror(errno));
        }

        if(n == 0) {
            return 0;
        }
        // fall through
    }

    // ::recv() through socket
    int result = ::recv(sock, (char *)data,length,0);

    if(result == SOCKET_ERROR) {
        throw TransportCannotReceive(ERS_HERE, strerror(errno));
    }

    return (size_t )result;
}


size_t Transport::receive(void *data, size_t length)
{
    struct iovec vec[1];
    vec[0].iov_base = data;
    vec[0].iov_len  = length;

    return receive(1, vec);
}


const NetAddress& Transport::address() const
{
    return m_addr;
}



bool Transport::can_send() const
{
    pollfd fd = { get_handle(), POLLOUT, 0 };
    return poll(&fd, 1, 0) == 1 && (fd.revents & POLLOUT);
}

bool Transport::can_receive() const
{
    pollfd fd = { get_handle(), POLLIN, 0 };
    return poll(&fd, 1, 0) == 1 && (fd.revents & POLLIN);
}


unsigned short Transport::local_port()
{
    struct sockaddr_in addr;
    socklen_t len = sizeof(addr); 
    SOCKET s = get_handle();
    getsockname(s, (struct sockaddr *)&addr, &len);
    return ntohs(addr.sin_port);
}

void Transport::set_send_buffer(size_t bufsize)
{
    setsockopt(get_handle(), SOL_SOCKET, SO_SNDBUF, 
               &bufsize,
               sizeof(bufsize));
}

size_t Transport::get_send_buffer() const
{
    size_t buflen = 0;
    socklen_t optlen = sizeof(buflen);
    getsockopt(get_handle(), SOL_SOCKET, SO_SNDBUF,
               &buflen,
               &optlen);
    return buflen;
}

void Transport::set_receive_buffer(size_t bufsize)
{
    setsockopt(get_handle(), SOL_SOCKET, SO_RCVBUF, 
               &bufsize,
               sizeof(bufsize));
}

size_t Transport::get_receive_buffer() const
{
    size_t buflen = 0;
    socklen_t optlen = sizeof(buflen);
    getsockopt(get_handle(), SOL_SOCKET, SO_RCVBUF,
               &buflen,
               &optlen);
    return buflen;
}
