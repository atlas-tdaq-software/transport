//
// $Id$
//

#include <iostream>
#include <errno.h>
#include <cstdio>
#include <string.h>
#include <sys/ioctl.h>
#include <poll.h>
#include "transport/SocketDefs.h"
#include "transport/NetAddress.h"
#include "transport/UDP.h"
#include "transport/Interface.h"

using namespace transport;
using namespace daq::transport;

void UDP::common_bind(int af)
{
    char service[32];
    std::sprintf(service, "%d", m_port);
    addrinfo hints = { AI_PASSIVE | AI_ADDRCONFIG, af, SOCK_DGRAM, IPPROTO_UDP, 0, 0, 0, 0 };
    addrinfo *result = 0;

    int err = getaddrinfo(0, service, &hints, &result);

    if(err != 0) {
        throw TransportCannotCreate(ERS_HERE, gai_strerror(err));
    }

    if(result == 0) {
        throw TransportCannotCreate(ERS_HERE, "Invalid arguments");
    }

    m_socket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);

    if(m_socket == INVALID_SOCKET) {
        freeaddrinfo(result);
        throw TransportCannotCreate(ERS_HERE, strerror(errno));
    }

    int no = 0;
    setsockopt (m_socket, SOL_IPV6, IPV6_V6ONLY, &no, sizeof(no));
    
    // if local_port != 0 bind socket
    if(m_port != 0) {
        
        // first set socket option to reuse address
        int option = 1;
        setsockopt(m_socket, SOL_SOCKET, SO_REUSEADDR,
                   (char *)&option, sizeof(option));

        if(bind(m_socket,
                result->ai_addr,
                result->ai_addrlen) == SOCKET_ERROR){
	    closesocket(m_socket);
            freeaddrinfo(result);
            throw TransportCannotBind(ERS_HERE, strerror(errno), m_port);
        }
    }

    freeaddrinfo(result);
}

UDP::UDP(const NetAddress& addr, unsigned short local_port)
    : Transport(addr), m_port(local_port)
{

    // connect socket to 'addr'
    struct sockaddr_storage saddr;
    addr.getsockaddr(saddr);

    common_bind(saddr.ss_family);

    if(connect(m_socket, 
               (struct sockaddr *)&saddr, 
               sizeof(saddr)) == SOCKET_ERROR) {
        throw TransportCannotConnect(ERS_HERE, strerror(errno), addr.hostname(), addr.port());
    }
}

UDP::UDP(unsigned short local_port)
    : Transport(NetAddress("",0)), m_port(local_port)
{

    common_bind(AF_UNSPEC);
}

UDP::~UDP()
{
    // ignore errors
    closesocket(m_socket);
}

void UDP::send_to(const void *data, size_t length, const NetAddress& receiver)
{
    struct sockaddr_storage saddr;
    receiver.getsockaddr(saddr);

    // send through socket to destination
    if(::sendto(m_socket,
                (const char *)data, 
                length, 
                MSG_NOSIGNAL, 
                (struct sockaddr *)&saddr, 
                sizeof(saddr)) == SOCKET_ERROR)
        throw TransportCannotSend(ERS_HERE, strerror(errno));
}

size_t UDP::recv_from(void *data, size_t length, NetAddress *source)
{
    sockaddr_storage saddr;
    socklen_t   addr_length = sizeof(saddr);

    // receive and get source
    int result = 0;
    do {
	result = ::recvfrom(m_socket, 
                            (char *)data, 
                            length, 
                            0, 
                            (struct sockaddr *)&saddr, 
                            &addr_length);
    } while(result == SOCKET_ERROR && (errno == EINTR || errno == EAGAIN));
    
    if(result == SOCKET_ERROR) {
        throw TransportCannotReceive(ERS_HERE, strerror(errno));
    }

    if(source != 0) {
        *source = NetAddress(saddr);
    }
    
    return (size_t )result;
}

bool UDP::set_multicast_if(const NetAddress& interface)
{
    struct sockaddr_storage maddr;
    interface.getsockaddr(maddr);

    if(maddr.ss_family == AF_INET) {
        // set the outgoing multicast interface
        struct ip_mreqn req;
        
        req.imr_address.s_addr = htonl(interface.inet_addr());
        req.imr_ifindex        = 0;
        
        return setsockopt(m_socket, SOL_IP, IP_MULTICAST_IF, &req, sizeof(req)) == 0;

    } else if(maddr.ss_family == AF_INET6) {

        // set the outgoing multicast interface
        int req = 0;
        
        return setsockopt(m_socket, SOL_IP, IPV6_MULTICAST_IF, &req, sizeof(req)) == 0;
        
    } 
    return false;
}

bool UDP::set_multicast_if(int interface, int af)
{
    if(af == AF_INET) {
        struct ip_mreqn req;
        memset(&req, 0, sizeof(req));
        req.imr_ifindex = interface;
        return setsockopt(m_socket, SOL_IP, IP_MULTICAST_IF, &req, sizeof(req)) == 0;

    } else if (af == AF_INET) {
        return setsockopt(m_socket, SOL_IP, IPV6_MULTICAST_IF, &interface, sizeof(interface)) == 0;        
    } else {
        return false;
    }
}

bool UDP::set_multicast_if(const std::string& name, int af)
{
    if(const Interface *intf = Interface::find(name, af)) {
        return set_multicast_if(intf->index(), af);
    }
    return false;
}

// join a multicast group, port in 'addr' is not considered
bool UDP::join_multicast(const NetAddress& addr)
{
    struct sockaddr_storage maddr;
    addr.getsockaddr(maddr);
    
    if(maddr.ss_family == AF_INET) {
        struct ip_mreqn req;
        int i = 1;
        
        do {
            req.imr_multiaddr.s_addr = htonl(addr.inet_addr());
            req.imr_address.s_addr   = htonl(INADDR_ANY);
            req.imr_ifindex          = i++;
        } while(setsockopt(m_socket, SOL_IP, IP_ADD_MEMBERSHIP, &req, sizeof(req)) == 0);

        return (errno != ENODEV);

    } else if(maddr.ss_family == AF_INET) {
        struct ipv6_mreq req;
        int i = 1;
        
        do {
            req.ipv6mr_multiaddr = ((sockaddr_in6*)&maddr)->sin6_addr;
            req.ipv6mr_interface = i++;
        } while(setsockopt(m_socket, SOL_IP, IPV6_ADD_MEMBERSHIP, &req, sizeof(req)) == 0);

        return (errno != ENODEV);

    } else {
        return false;
    }
}

bool UDP::join_multicast(const NetAddress& addr, const NetAddress& network)
{
    using daq::transport::Interface;

    struct sockaddr_storage maddr;
    addr.getsockaddr(maddr);

    if(maddr.ss_family == AF_INET) {
    
        struct ip_mreqn req;
        
        const Interface::InterfaceList& list = Interface::interfaces();
        for(Interface::InterfaceList::const_iterator it = list.begin();
            it != list.end();
            ++it) {
            if( (*it)->has_flag(IFF_UP) && ((*it)->address() & (*it)->netmask()) == htonl(network.inet_addr())) {
                req.imr_multiaddr.s_addr = htonl(addr.inet_addr());
                req.imr_address.s_addr   = (*it)->address();
                req.imr_ifindex          = (*it)->index();
                return (setsockopt(m_socket, SOL_IP, IP_ADD_MEMBERSHIP, &req, sizeof(req)) == 0);
            } 
        }

    } else if(maddr.ss_family == AF_INET6) {
        struct ipv6_mreq req;
        
        const Interface::InterfaceList& list = Interface::interfaces();
        for(Interface::InterfaceList::const_iterator it = list.begin();
            it != list.end();
            ++it) {
            if( (*it)->has_flag(IFF_UP) && ((*it)->network_v6() == network.inet_addr_v6()) ) {
                req.ipv6mr_multiaddr    = ((sockaddr_in6*)&maddr)->sin6_addr;
                req.ipv6mr_interface    = (*it)->index();
                return (setsockopt(m_socket, SOL_IP, IPV6_ADD_MEMBERSHIP, &req, sizeof(req)) == 0);
            } 
        }

    }
    
    return false;
}


// leave a multicast group, port in 'addr' is not considered
bool UDP::leave_multicast(const NetAddress& addr)
{
    struct sockaddr_storage maddr;
    addr.getsockaddr(maddr);

    if(maddr.ss_family == AF_INET) {
        struct ip_mreq req;
        
        req.imr_multiaddr.s_addr = htonl(addr.inet_addr());
        req.imr_interface.s_addr = htonl(INADDR_ANY);
        return setsockopt(m_socket, SOL_IP, IP_DROP_MEMBERSHIP, &req, sizeof(req)) == 0;        
    } else if (maddr.ss_family == AF_INET6) {
        struct ipv6_mreq req;        
        req.ipv6mr_multiaddr = ((sockaddr_in6*)&maddr)->sin6_addr;
        return setsockopt(m_socket, SOL_IP, IPV6_DROP_MEMBERSHIP, &req, sizeof(req)) == 0;        
    }

    return false;
}

SOCKET UDP::get_handle() const
{
    return m_socket;
}

size_t UDP::message_length() const
{
    int length = 0;
    return (ioctl(m_socket, FIONREAD, & length) != -1) ? length : 0;
}

