
#include <stdio.h>
#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include "transport/SigioManager.h"

namespace daq {
    namespace transport {

        SigioManager::SigioManager(int signum)
            : PollManager(),
              m_signal(signum),
              m_pid(0),
              m_sig_fd(-1)
        {
            sigemptyset(&m_signal_set);
            sigaddset(&m_signal_set, m_signal);
            sigaddset(&m_signal_set, SIGIO);
            sigprocmask(SIG_BLOCK, &m_signal_set, NULL);
            m_pid = getpid();
        }

        SigioManager::~SigioManager()
        {
            // not sure about this...
            sigprocmask(SIG_UNBLOCK, &m_signal_set, NULL);
        }

        void SigioManager::add_handle(SOCKET s, Mode mode)
        {
            // need better error handling here...
            int flags;

            if(fcntl(s, F_GETFL, &flags) < 0) {
                return;
            }

            flags |= O_NONBLOCK | O_ASYNC;

            if(fcntl(s, F_SETFL, flags) < 0) {
                return;
            }

            if(fcntl(s, F_SETSIG, m_signal) < 0) {
                return;
            }

            if(fcntl(s, F_SETOWN, m_pid) < 0) {
                return;
            }

            PollManager::add_handle(s, mode);
        }

        void SigioManager::remove_handle(SOCKET s)
        {
            int flags;

            if(fcntl(s, F_GETFL, &flags) < 0) {
                return;
            }

            flags &= ~(O_NONBLOCK | O_ASYNC);

            if(fcntl(s, F_SETFL, flags) < 0) {
                return;
            }

            // clear the signal queue
            signal(m_signal, SIG_IGN);
            signal(m_signal, SIG_DFL);

            PollManager::remove_handle(s);
        }

        // check if any socket is ready
        bool SigioManager::ready()
        {
            if(m_num_ready > 0) {
                // PollManager still has ready descriptors
                return true;
            }

            if(m_sig_fd != -1) {
                // somebody else called ready() before
                return true;
            }

            m_sig_fd = wait_sig(0);

            return m_sig_fd != SOCKET_ERROR;
        }

        // waiting for data, returns SOCKET_ERROR if none available
        SOCKET SigioManager::next()
        {
            if(m_num_ready > 0) {
                // poll() has still file descriptors
                return PollManager::next();
            }

            if(m_sig_fd != -1) {
                // somebody called ready() before 
                // and we have a file descriptor
                SOCKET result = m_sig_fd;
                m_sig_fd = -1;
                return result;
            }

            return wait_sig(-1);
        }


        // timeout is in microseconds
        SOCKET SigioManager::next(unsigned int timeout)
        {
            if(m_num_ready > 0) {
                // poll() has still file descriptors
                return PollManager::next(timeout);
            }

            if(m_sig_fd != -1) {
                // somebody called ready() before 
                // and we have a file descriptor
                SOCKET result = m_sig_fd;
                m_sig_fd = -1;
                return result;
            }

            return wait_sig(timeout);
        }

        SOCKET SigioManager::wait_sig(int timeout)
        {

            siginfo_t info;
            int result;

            // wait for a realtime signal
            if(timeout < 0) {
                // wait forever
                result = sigwaitinfo(&m_signal_set, &info);
            } else {
                struct timespec ts;
                ts.tv_sec  = 0;
                ts.tv_nsec = timeout * 1000;
                result = sigtimedwait(&m_signal_set, &info, &ts);
            }

            if(result < 0) {
                return SOCKET_ERROR;
            } else if(result == SIGIO) {
                // real time queue overflow
                printf("overflow\n");

                // this should clear the queue...
                signal(m_signal, SIG_IGN);

                // but can't leave it ignored ??
                signal(m_signal, SIG_DFL);

                // do a normal poll();
                // this sets the m_num_ready in the parent object
                PollManager::wait_poll(timeout);
                return PollManager::get_next();

            } else {
                // printf("got a signal !\n");
                // this should always be our realtime signal
                // if(result != m_signal) abort();
                return info.si_fd;
            }
    
        }
    }
}
