//
// $Id$
//

//
// TCP client test program
//
// usage: 
//   test_tcpclnt <hostname> [ 0 | 1 ]
//

#include <string>
#include <iostream>

#include "transport/NetAddress.h"
#include "transport/Transport.h"
#include "transport/SCTP.h"

using namespace std;

int main(int argc, char *argv[])
{
  // default remote host is localhost
  string remote_host = "localhost";

  if(argc > 1)
    remote_host = argv[1];

  // specify server address, assume local host
  NetAddress addr(remote_host.c_str(), 9753);

  // create client side 
  SCTP transport(0, false);

  cout << "Type END to quit server" << endl;

  // loop until END input given
  while(true) {
    string buffer;

    // get string from standard input
    cout << "Input: ";
    if(cin >> buffer) {
	// send data
	struct iovec vec;
	vec.iov_base = const_cast<char *>(buffer.c_str());
	vec.iov_len  = buffer.size();
	if(!transport.send(1, &vec, addr)) {
	    std::cout << "Could not send data !" << std::endl;
	}
	
	// check if end reached
	if(buffer == "END")
	    break;
    } else {
	break;
    }
    
  }
  
  
  return 0;

}
