//
// $Id$
//

//
// TCP client test program
//
// usage: 
//   test_tcpclnt <hostname> [ 0 | 1 ]
//

#include <string>
#include <iostream>

#include "transport/NetAddress.h"
#include "transport/Transport.h"
#include "transport/SCTP.h"

using namespace std;

int main(int argc, char *argv[])
{
  // default remote host is localhost
  string remote_host = "localhost";

  if(argc > 1)
    remote_host = argv[1];

  // specify server address, assume local host
  NetAddress addr(remote_host.c_str(), 9753);

  try {

      // create client side 
      SCTP transport(addr);
      
      cout << "Type END to quit server" << endl;
      
      // loop until END input given
      while(true) {
          string buffer;
          
          // get string from standard input
          cout << "Input: ";
          if(cin >> buffer) {
              // send data
              if(!transport.send(buffer.c_str(), buffer.size())) {
                  std::cout << "Could not send data !" << std::endl;
              }
              
              // check if end reached
              if(buffer == "END")
                  break;
          } else {
              break;
          }
          
      }
  } catch (ers::Issue& ex) {
      cerr << "Exception caught: " << ex << '\n';
  }
  
  return 0;

}
