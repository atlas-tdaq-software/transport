
#include <iostream>
#include "transport/Interface.h"

int main()
{
    using namespace daq::transport;

    const Interface::InterfaceList& list = Interface::interfaces();

    std::cout << "Found " << list.size() << " interfaces: " << std::endl;

    for(Interface::InterfaceList::const_iterator it = list.begin();
        it != list.end();
        ++it) {
        std::cout << std::hex;
        std::cout << "Name:\t" << (*it)->name() << std::endl;
        std::cout << "Index:\t" << (*it)->index() << std::endl;
        std::cout << "Flags:\t" << (*it)->flags();
        if((*it)->has_flag(IFF_UP)) std::cout << " UP ";
        if((*it)->has_flag(IFF_LOOPBACK)) std::cout << " LOOPBACK ";
        if((*it)->has_flag(IFF_BROADCAST)) std::cout << " BROADCAST ";
        if((*it)->has_flag(IFF_MULTICAST)) std::cout << " MULTICAST ";
        std::cout << std::dec << "Address: " << ' ' << *(*it) << std::endl;
        if((*it)->family() == AF_INET) {
            std::cout << "Broadcast:\t" << (*it)->broadcast() << std::endl;
            std::cout << "Broadcast:\t" << (*it)->broadcast_string() << std::endl;
        }

        std::cout << std::endl;

    }
}
