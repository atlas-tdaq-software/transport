
//
// common test routines
//

#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include "test_manager.h"

int test_setup(daq::transport::SocketManager *mgr, int n)
{
    std::list<TCP*>    trans;
    std::list<Sender*> sender;

    create_receivers(n, trans);

    if(fork() == 0) {

	// close receivers
	for(std::list<TCP*>::iterator it = trans.begin();
	    it != trans.end();
	    ++it) {
	    delete *it;
	}

	// create senders
	for(int i = 0; i < n; i++) {
	    sender.push_back(new Sender(9123 + i, ('a' + (i % 64))));
	}

	// send data
	for(int i = 0; i < 1024; i++) {
	    for(std::list<Sender*>::iterator it = sender.begin();
		it != sender.end();
		++it) {
		(*it)->send();
	    }
	}

	for(std::list<Sender*>::iterator it = sender.begin();
	    it != sender.end();
	    ++it) {
	    delete (*it);
	}

	exit(0);

    } else {

	for(std::list<TCP*>::iterator it = trans.begin();
	    it != trans.end();
	    ++it) {
	    Transport *t = (*it)->accept();
	    if(t) {
		mgr->add_handle(t->get_handle());
	    } else {
		perror("Accept failed");
	    }
	    delete (*it);
	}
    }

    return n;
}

int test_manager(daq::transport::SocketManager *mgr, int n)
{
    char buffer[65];

    while(n > 0) {
        SOCKET s = mgr->next();
        if(s != SOCKET_ERROR) {

	    int res = ::recv(s, buffer ,sizeof(buffer)-1, MSG_WAITALL);

            if(res < 0) {
                perror("recv");
                return n;
            }

            if(res == 0) {
                std::cout << "Removing (EOF): " << s << std::endl;
                mgr->remove_handle(s);
                ::close(s);
                n--;
            } else {
		assert(res == 64);
                buffer[res] = '\0';
                // std::cout << "Received from " << s << ":" << std::endl;
                // std::cout << buffer << std::endl;
            }

        } else {
            return n;
        }
    }
    
    return 0;
}

int create_receivers(int n, std::list<TCP*>& trans)
{
    unsigned short base = 9123;

    for(int i = 0; i < n; i++) {
        TCP *t = new TCP(base + i);
        trans.push_back(t);
    }

    return n;
}


Sender::Sender(unsigned short port, char c)
{
    m_tcp = new TCP(NetAddress("localhost", port));
    for(int i = 0; i < 64; i++) {
        m_char[i] = c;
    }
}

Sender::~Sender()
{
    delete m_tcp; 
}
 

void Sender::send()
{
    for(int i = 0; i < 10; i++) {
        if(!m_tcp->send(m_char, sizeof(m_char))) {
	    std::cout << "Cannot send" << std::endl;
	    break;
	}
    }
}

