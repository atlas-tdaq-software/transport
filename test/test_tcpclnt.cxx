//
// $Id$
//

//
// TCP client test program
//
// usage: 
//   test_tcpclnt <hostname> [ 0 | 1 ]
//

#include <string>
#include <iostream>

#include "transport/NetAddress.h"
#include "transport/Transport.h"
#include "transport/TCP.h"

using namespace std;

int main(int argc, char *argv[])
{
  // default remote host is localhost
  string remote_host = "localhost";

  bool nodelay = false;

  if(argc > 1)
    remote_host = argv[1];

  if(argc > 2)
      nodelay = (strtol(argv[2], 0,0) != 0);

  // specify server address, assume local host
  NetAddress addr(remote_host.c_str(), 9753);

  // create client side TCP port
  try {
      TCP transport(addr, 0, 100000);

      if(!transport.set_nodelay(nodelay)) {
          std::cerr << "Cannot set the TCP_NODELAY option !" << std::endl;
      }
      
      cout << "Type END to quit server" << endl;
      
      // loop until END input given
      while(true) {
          string buffer;
          
          // get string from standard input
          cout << "Input: ";
          cin >> buffer;
          
          // send data
          if(!transport.send(buffer.c_str(), buffer.size())) {
              std::cout << "Could not send data !" << std::endl;
          }
          
          // check if end reached
          if(buffer == "END")
              break;
      }
  } catch (ers::Issue& ex) {
      cout << "Exception caught: " << ex << endl;
  }

  return 0;

}
