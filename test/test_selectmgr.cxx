
#include "test_manager.h"
#include "transport/SelectManager.h"

const int NUM_TRANSPORT = 16;

int main(int argc, char *argv[])
{
    int num = NUM_TRANSPORT;

    if(argc > 1) 
        num = strtol(argv[1], 0, 0);

    daq::transport::SelectManager *mgr = new daq::transport::SelectManager;

    test_setup(mgr, num);
    test_manager(mgr, num);
}
