//
// $Id$
//

//
// SCTP server test program
//

#include <stdlib.h>
#include <string.h>

#include <iostream>

#include "transport/Transport.h"
#include "transport/SCTP.h"

using namespace std;

static SCTP *make_connection(bool stream)
{
    if(stream) {
	// create server side one-to-many port
	SCTP listener(9753);
	
	// wait for connection
	SCTP *transport = listener.accept();
	
	NetAddress remote = transport->address();
	
	std::cout << "Accepted connection from host: " 
		  << remote.hostname() << " port: "
		  << remote.port() << std::endl;
	return transport;
	
    } else {
	return new SCTP(9753, false);
    }
}

int main(int argc, char *argv[])
{
    
    SCTP *transport = make_connection(argc > 1);
    
    // loop until special packet received
    while(true) {
        char buffer[1024];
        
        memset(buffer, 0, sizeof(buffer));
        
        // receive data
	size_t len = 0;
	try {
	    len = transport->receive(buffer,sizeof(buffer));
	} catch (transport::TransportException& ex) {
	    cerr << "Got exception: " << ex.what() << endl;
	}
        
        if(len == 0) {
            cerr << "connection broken" << endl;
            exit (EXIT_FAILURE);
        }
        
        // check if end reached
        if(strncmp(buffer, "END", 3) == 0)
            break;
        
        // else print output
        cout << "SCTP server: " << buffer << endl;
    }
    
    delete transport;
    
    return (EXIT_SUCCESS);

}
