
#include "transport/Network.h"
#include <iostream>

int main()
{
    using namespace std;
    using namespace daq::transport;

    try {
        Network nw1("192.168.1.100");
        Network nw2("192.168.1.101/24");
        Network nw3("192.168.1.102/16");
        Network nw4("192.168.1.103/255.255.252.0");
        Network nw5("10.147.12.30");
        Network nw6("10.147.12.30/24");

        cout << nw1 << endl << nw2 << endl << nw3 << endl << nw4 << endl << nw5 << endl << nw6 << endl;

        Network nw7("10.147.12.30/36");

    } catch(ers::Issue& ex) {
        cerr << "got exception: " << ex << endl;
    }
    
}
