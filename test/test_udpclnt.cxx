//
// $Id$
//

//
// UDP client test program
//

#include <string>
#include <iostream>

#include "transport/NetAddress.h"
#include "transport/Transport.h"
#include "transport/UDP.h"

using namespace std;

int main(int argc, char *argv[])
{
  // default remote host is our own machine
  string remote_host = "localhost";

  // first argument is remote host
  if(argc > 1)
    remote_host = argv[1];
    
  // second argument is flag to use send_to instread of send
  bool use_sendto = (argc > 2);

  // specify server address, assume local host
  NetAddress addr(remote_host.c_str(), 9876);


  try {

      // create client side UDP port
      UDP transport(addr, 0);
      
      cout << "Type END to quit server" << endl;
      
      // loop until END input given
      while(true) {
          string buffer;
          
          // get string from standard input
          cout << "Input: ";
          cin >> buffer;
          
          // send data
          if(use_sendto) {
              transport.send_to(buffer.c_str(), buffer.size(), addr);
          } else {
              transport.send(buffer.c_str(), buffer.size());
          }
          
          // check if end reached
          if(buffer == "END")
              break;
      }

  } catch (ers::Issue& ex) {
      cerr << "Caught exception: " << ex << '\n';
  }
  return 0;

}
