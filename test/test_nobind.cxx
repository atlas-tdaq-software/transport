//
// $Id$
//

//
// Check if passing a 0 as port number for a server will
// automatically bind the address when we ask for it.
//
// usage:
//      test_nobind
//

#include <stdlib.h>
#include <string.h>

#include <iostream>

#include "transport/Transport.h"
#include "transport/UDP.h"
#include "transport/TCP.h"

using namespace std;

int main()
{

    {
	// create server side UDP port
	UDP transport(0);
	
	unsigned short port = transport.local_port();
	cout << "UDP port = " << port << endl;
    }

    {
	// create server side TCP port
	TCP transport(0);
	
	unsigned short port = transport.local_port();
	cout << "TCP port = " << port << endl;
    }
    
    return (EXIT_SUCCESS);

}
