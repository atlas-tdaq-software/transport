
#include <stdlib.h>
#include <iostream>

#include <chrono>

#include "transport/Transport.h"
#include "transport/NetAddress.h"
#include "transport/UDP.h"

const unsigned short PORT     = 8192;
const int            MAX_SIZE = 8192;
const int            COUNT    = 10000;

static int    sizes[] = { 
    64, 
    128, 
    256, 
    512, 
    1024,
    2048,
    4096, 
    8192 
};

static void do_receive(char *buf, size_t length, UDP& conn)
{
    size_t len = conn.receive(buf, length);
    if(len != length) {
        std::cerr << "received wrong length: " << len << std::endl;
        exit(EXIT_FAILURE);
    }
}

int main(int argc, char *argv[])
{
    try {
        if(argc > 2) {
            
            // we are the server
            NetAddress addr(argv[1], PORT);
            UDP  conn(addr, PORT);
            char buffer[MAX_SIZE];
            
            for(size_t i = 0; i < sizeof(sizes)/sizeof(sizes[0]); i++) {
                for(int count = 0; count < COUNT; count++) {
                    // read all data we expect, may have to loop here...
                    do_receive(buffer, sizes[i], conn);
                    // send data back
                    conn.send(buffer, sizes[i]);
                }
            }
            
        } else {
            // we are the client
            char buffer[MAX_SIZE];
            
            NetAddress addr(argv[1], PORT);
            UDP conn(addr,PORT);
            std::cout << "SIZE\tTIME(us)\tBANDWTH(MByte/s)" << std::endl;
            for(size_t i = 0; i < sizeof(sizes)/sizeof(sizes[0]); i++) {
                auto start = std::chrono::high_resolution_clock::now();
                for(int count = 0; count < COUNT; count++) {
                    conn.send(buffer, sizes[i]);
                    do_receive(buffer, sizes[i], conn);
                }
                std::chrono::duration<double, std::micro> diff = std::chrono::high_resolution_clock::now() - start;
                std::cout << sizes[i] << "\t" 
                          << diff.count()/COUNT << "\t"
                          <<  COUNT*sizes[i]/diff.count()
                          << " MByte/s" << std::endl;
		// do_receive(buffer, 16, &conn);
            }

            
        }
    } catch(TransportException& exc) {
        std::cout << "TransportException raised: " << exc.message() << std::endl;
        exit(EXIT_FAILURE);
    } catch(...) {
        std::cout << "Unknown exception raised: ";
        exit(EXIT_FAILURE);
    }

    return (EXIT_SUCCESS);
}
