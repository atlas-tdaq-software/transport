//
// $Id$
//

//
// TCP server test program
//

#include <stdlib.h>
#include <string.h>

#include <iostream>

#include "transport/Transport.h"
#include "transport/TCP.h"

using namespace std;

int main(int argc, char *argv[])
{
    bool nodelay = false;

    if(argc > 1) {
        nodelay = (strtol(argv[1], 0, 0) != 0);
    }
    
    // create server side UDP port
    TCP listener(9753);
    
    // wait for connection

    while(true) {
        TCP *transport = listener.accept();

        NetAddress remote = transport->address();
        
        std::cout << "Accepted connection from: " << remote << std::endl;
        
        if(!transport->set_nodelay(nodelay)) {
            std::cerr << "Cannot TCP_NODELAY option !" << std::endl;
        }
        
        // loop until special packet received
        while(true) {
            char buffer[1024];
            
            memset(buffer, 0, sizeof(buffer));
            
            // receive data
            size_t len = transport->receive(buffer,sizeof(buffer));
            
            if(len == 0) {
                cerr << "connection broken" << endl;
                break;
            }
            
            // check if end reached
            if(strncmp(buffer, "END", 3) == 0) {
                break;
            }
            
            // else print output
            cout << "TCP server: " << buffer << endl;
        }
    
        delete transport;

    }
    
    return (EXIT_SUCCESS);

}
