
#include <stdlib.h>
#include "test_manager.h"
#include "transport/EpollManager.h"

const int NUM_TRANSPORT = 16;

int main(int argc, char *argv[])
{
    int num = NUM_TRANSPORT;

    if(argc > 1) 
        num = strtol(argv[1], 0, 0);

    daq::transport::EpollManager *mgr = new daq::transport::EpollManager(num);
    test_setup(mgr, num);
    test_manager(mgr, num);
}
