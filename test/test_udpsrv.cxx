//
// $Id$
//

//
// UDP server test program
//
// usage:
//      test_udpsrv [ use_recvfrom [ multicast_address ] ] 
//
//         'use_rcvfrom'        is 0 or non-zero (default: zero)
//
//         'multicast_address'  is a multicast address to join
//

#include <stdlib.h>
#include <string.h>

#include <iostream>

#include "transport/Transport.h"
#include "transport/UDP.h"

using namespace std;

int main(int argc, char *argv[])
{
    bool use_recvfrom = false;

    if(argc > 1) {
        use_recvfrom = (strtol(argv[1], 0, 0) != 0);
    }

    // create server side UDP port
    UDP transport(9876);
    
    if(argc > 2) {
        if(!transport.join_multicast(NetAddress(argv[2], 0))) {
            std::cerr << "Cannot join multicast group" << std::endl;
        }
    }

    // loop until special packet received
    while(true) {
        NetAddress addr("",0);
        char buffer[1024];
        size_t len;

        memset(buffer, 0, sizeof(buffer));
        
        // receive data
        if(use_recvfrom) {
            len = transport.recv_from(buffer,sizeof(buffer), &addr);
        } else {
            len = transport.receive(buffer,sizeof(buffer));
        }
        
        if(len == 0) {
            std::cerr << "Message of length zero received" << endl;
            exit(EXIT_FAILURE);
        }

        if(use_recvfrom) {
            std::cout << "Received Message from: " << addr << std::endl;
        }
        
        // check if end reached
        if(strncmp(buffer, "END", 3) == 0)
            break;
        
        // else print output
        std::cout << "UDP server: " << buffer << endl;
    }
    
    return (EXIT_SUCCESS);

}
