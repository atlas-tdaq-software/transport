//
// $Id$
//

//
// TCP client test program
//
// usage: 
//   test_unixclnt <path>
//

#include <string>
#include <iostream>

#include "transport/NetAddress.h"
#include "transport/Transport.h"
#include "transport/Unix.h"

using namespace std;

int main()
{

    using namespace daq::transport;

    // default remote host is localhost
    string path = "Test";
    
    // create client side TCP port
    Unix transport(path);

    cout << "Type END to quit server" << endl;
    
    // loop until END input given
    while(true) {
        string buffer;
        
        // get string from standard input
        cout << "Input: ";
        cin >> buffer;
        
        // send data
        if(!transport.send(buffer.c_str(), buffer.size())) {
            std::cout << "Could not send data !" << std::endl;
        }
        
        // check if end reached
        if(buffer == "END")
            break;
    }
    
    return 0;
    
}
