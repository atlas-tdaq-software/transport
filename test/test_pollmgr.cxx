
#include <stdlib.h>
#include "test_manager.h"
#include "transport/PollManager.h"

const int NUM_TRANSPORT = 16;

int main(int argc, char *argv[])
{
    int num = NUM_TRANSPORT;

    if(argc > 1) 
        num = strtol(argv[1], 0, 0);

    daq::transport::PollManager *mgr = new daq::transport::PollManager;
    test_setup(mgr, num);
    test_manager(mgr, num);
}
