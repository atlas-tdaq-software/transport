// this is -*- c++ -*-
#ifndef TRANSPORT_TESTMANAGER_H_
#define TRANSPORT_TESTMANAGER_H_

//
// NOTE: 
// To run the tests for the different socket managers
// (select, poll, epoll) with a large number of sockets,
// it is necessary to adjust some user limits, e.g.:
//
// ulimit -n 8192 
// ulimit -s 1024
//

#include <list>
#include "transport/TCP.h"
#include "transport/SocketManager.h"

//
// test a given socket manager
// read data from n available sockets until EOF reached
// then return. If EOF is reached on a socket, 
// remove it from the manager
//

int test_setup(daq::transport::SocketManager *mgr, int n);
int test_manager(daq::transport::SocketManager *mgr, int n);
int create_receivers(int n, std::list<TCP*>& trans);

class Sender {
public:
    Sender(unsigned short port, char c);
    ~Sender();
public:
    void send();
private:
    TCP *m_tcp;
    char m_char[64];
};

#endif // TRANSPORT_TESTMANAGER_H_
