//
// $Id$
//

//
// Unix server test program
//

#include <stdlib.h>
#include <string.h>

#include <iostream>

#include "transport/Transport.h"
#include "transport/Unix.h"

using namespace std;

int main()
{

    using namespace daq::transport;

    // create server side UDP port
    Unix listener("Test", true);
    
    // wait for connection
    Unix *transport = listener.accept();
    while(transport == 0) {
        perror("accept");
        sleep(1);
        transport = listener.accept();
    }

    // loop until special packet received
    while(true) {
        char buffer[1024];
        
        memset(buffer, 0, sizeof(buffer));
        
        // receive data
        size_t len = transport->receive(buffer,sizeof(buffer));
        
        if(len == 0) {
            cerr << "connection broken" << endl;
            exit (EXIT_FAILURE);
        }
        
        // check if end reached
        if(strncmp(buffer, "END", 3) == 0)
            break;
        
        // else print output
        cout << "Unix server: " << buffer << endl;
    }
    
    delete transport;
    
    return (EXIT_SUCCESS);

}
