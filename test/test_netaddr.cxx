
#include "transport/NetAddress.h"
#include <iostream>
#include <cstdlib>

int main(int argc, char *argv[])
{
    using namespace std;

    if(argc != 3) {
        cerr << "Usage: " << argv[0] << " hostname port\n";
        exit(1);
    }

    NetAddress addr(argv[1], strtol(argv[2],0,0));

    cout << addr << '\n';

    cout << "IPV4: " << addr.inet_addr() << '\n';

    sockaddr_storage saddr;
    if(addr.getsockaddr(saddr)) {
        cout << "address family:" << saddr.ss_family << '\n';
    }

    NetAddress addr2(INADDR_LOOPBACK, 1023);
    cout << addr2 << '\n';
}
