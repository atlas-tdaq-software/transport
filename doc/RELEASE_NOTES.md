# transport

Clients using classes from the `transport` package should
look into more modern network libraries like 
[boost::asio](https://www.boost.org/doc/libs/1_73_0/libs/asio/)
until the C++ standard contains an official network library.
