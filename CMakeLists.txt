
tdaq_package()

include(CheckIncludeFile)

set(CMAKE_REQUIRED_QUIET TRUE)
Check_Include_File(/usr/include/netinet/sctp.h HAVE_SCTP_H)

tdaq_add_library(transport
  src/NetAddress.cxx 		
  src/Transport.cxx 		
  src/TCP.cxx 		
  src/UDP.cxx		        
  src/Unix.cxx                
  src/SelectManager.cxx       
  src/PollManager.cxx         
  src/Interface.cxx           
  src/EpollManager.cxx        
  src/Network.cxx
  LINK_LIBRARIES tdaq-common::ers)

if(HAVE_SCTP_H)
  target_sources(transport PRIVATE src/SCTP.cxx)
endif()

tdaq_add_executable(test_pp        NOINSTALL test/test_pp.cxx        LINK_LIBRARIES transport)
tdaq_add_executable(test_ppudp     NOINSTALL test/test_ppudp.cxx     LINK_LIBRARIES transport)
tdaq_add_executable(test_stream    NOINSTALL test/test_stream.cxx    LINK_LIBRARIES transport)
tdaq_add_executable(test_tcpclnt   NOINSTALL test/test_tcpclnt.cxx   LINK_LIBRARIES transport)
tdaq_add_executable(test_tcpsrv    NOINSTALL test/test_tcpsrv.cxx    LINK_LIBRARIES transport)

if(HAVE_SCTP_H)
tdaq_add_executable(test_sctpclnt  NOINSTALL test/test_sctpclnt.cxx  LINK_LIBRARIES transport)
tdaq_add_executable(test_sctpclnt2 NOINSTALL test/test_sctpclnt2.cxx LINK_LIBRARIES transport)
tdaq_add_executable(test_sctpsrv   NOINSTALL test/test_sctpsrv.cxx   LINK_LIBRARIES transport)
endif()

tdaq_add_executable(test_udpclnt   NOINSTALL test/test_udpclnt.cxx   LINK_LIBRARIES transport)
tdaq_add_executable(test_udpsrv    NOINSTALL test/test_udpsrv.cxx    LINK_LIBRARIES transport)
tdaq_add_executable(test_if        NOINSTALL test/test_if.cxx        LINK_LIBRARIES transport)
tdaq_add_executable(test_nobind    NOINSTALL test/test_nobind.cxx    LINK_LIBRARIES transport)
tdaq_add_executable(test_unixclnt  NOINSTALL test/test_unixclnt.cxx  LINK_LIBRARIES transport)
tdaq_add_executable(test_unixsrv   NOINSTALL test/test_unixsrv.cxx   LINK_LIBRARIES transport)
tdaq_add_executable(test_network   NOINSTALL test/test_network.cxx   LINK_LIBRARIES transport)
tdaq_add_executable(test_netaddr   NOINSTALL test/test_netaddr.cxx   LINK_LIBRARIES transport)
