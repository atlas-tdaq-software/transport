// this is -*- c++ -*-
#ifndef TRANSPORT_POLLMANAGER_H_
#define TRANSPORT_POLLMANAGER_H_

#include "transport/SocketManager.h"

#include <vector>
#include <sys/poll.h>

struct pollfd;

namespace daq {
    namespace transport {

        //
        // An implementation of the SocketManager interface
        // using the poll() function. Should work on all
        // Unix systems which have the poll interface.
        //

        class PollManager : public SocketManager {
        public:
            PollManager();
            virtual ~PollManager();
        public:                         // changing the socket set
            virtual void    add_handle(SOCKET s, Mode mode) override;
            virtual void    remove_handle(SOCKET s) override;

        public:                         
            // check if any socket is ready
            virtual bool   ready() override;

            // waiting for data, returns SOCKET_ERROR if none available
            virtual SOCKET next() override;

            // timeout is in microseconds
            virtual SOCKET next(unsigned int timeout) override;

        protected:

            void wait_poll(int timeout);
            SOCKET get_next();


            // number of ready file descriptors
            int            m_num_ready;

            // the number of free entries in the poll array
            int            m_free_fd;

            // poll structure array
            std::vector<pollfd> m_poll;

        };
    }
}

using daq::transport::PollManager;

#endif // TRANSPORT_POLLMANAGER_H_
