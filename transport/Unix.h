// this is -*- c++ -*-
#ifndef TRANSPORT_UNIX_H_
#define TRANSPORT_UNIX_H_

#include "transport/Transport.h"

namespace daq {

    namespace transport {

        class Unix : public Transport {
        public:
            /// Create socket and connect to it, or bind() if
            /// if 'server' is true.
            Unix(const std::string& path, bool server = false);
            virtual ~Unix();
        public:
            /// get underlying SOCKET
            virtual SOCKET get_handle() const;
            
            /// return local port number
            virtual unsigned short local_port();            

            /// Accept a new connection, if server
            Unix *accept();

        private:

            Unix(SOCKET new_socket);

            SOCKET m_socket;
            bool   m_server;
        };

    }

}

#endif // TRANSPORT_UNIX_H_
