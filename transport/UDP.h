/* this is -*- C++ -*- */
#ifndef TRANSPORT_UDP_H_
#define TRANSPORT_UDP_H_

//
// $Id$
//

#include "transport/Transport.h"
#include "transport/SocketDefs.h"

class NetAddress;

/*
 * Interface to UDP transport protocol.
 *
 * The UDP class provides an interface to the UDP/IP protocol. UDP
 * provides an extended interface where one can specify the destination
 * address explicitly in a send().
 */
class UDP : public Transport {

public:
    /* Create connection to specified NetAddress. Bind local transport
     * if 'local_port' is not zero.
     * Throws:
     * 
     *  TransportCannotCreate
     *  TransportHostNotFound
     *  TransportCannotBind()
     *  TransportCannotConnect() 
     * 
     */
    UDP(const NetAddress& addr, unsigned short local_port = 0);

    /* Create connection without specified end-point. Bind local transport
     * if 'local_port' is not zero.
     * Throws:
     * 
     *  TransportCannotCreate
     *  TransportHostNotFound
     *  TransportCannotBind() 
     * 
     */
    explicit UDP(unsigned short local_port = 0);
    ~UDP();
public:
    /// Extended send() interface. Specify 'receiver' explicitly in every call.
    void send_to(const void *data, size_t length, const NetAddress& receiver);

    /** Extended receive() interface. If 'source' is not NULL, fill it with
     * the address from where the data comes from.
     */
    size_t recv_from(void *data, size_t length, NetAddress *source);

    //
    // UPD socket options
    //

    /// join a multicast group, port in 'addr' is not considered, join on all interfaces
    bool join_multicast(const NetAddress& addr);

    /// join a multicast group, only on interface corresponding to to 'network'
    bool join_multicast(const NetAddress& addr, const NetAddress& network);

    /// leave a multicast group, port in 'addr' is not considered
    bool leave_multicast(const NetAddress& addr);

    /// set interface for outgoing multicast packets, port is not considered
    bool set_multicast_if(const NetAddress& interface);

    /// set interface for outgoing multicast packets using index
    bool set_multicast_if(int interface, int af);

    /// set interface for outgoing multicast packets using name
    bool set_multicast_if(const std::string& name, int af);

    /// return underlying SOCKET object
    SOCKET get_handle() const;

    /// return size of next message in receive buffer or 0 if none available.
    size_t message_length() const;

private:
    void common_bind(int af);

private:
    unsigned short m_port;
    SOCKET         m_socket;
};

#endif // TRANSPORT_UDP_H_
