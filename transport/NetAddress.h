/* this is -*- C++ -*- */
#ifndef TRANSPORT_NETADDRESS_H_
#define TRANSPORT_NETADDRESS_H_

//
// $Id$
//

#include <string>

#include "transport/Types.h"

#include <netinet/in.h>
#include <iosfwd>

/*
 * Internet address plus port number.
 *
 * A NetAddress represents an IP address plus a port number, thereby
 * specifying a typical service access point e.g. for TCP or UDP.
 *
 * The 'hostname' argument can be a numerical dotted decimal IPV4 address or
 * or a colon separated IPV6 address.
 */

class NetAddress {
public:
    /// Create NetAddress with remote hostname and port number.
    ///
    /// 'hostname' can be a numeric string in either IPV4 or IPV6 format.
    ///
    NetAddress(const std::string& hostname, daq::transport::IPPort port);

    // Create NetAddress from IP address, 'addr' is in host byte order !
    NetAddress(daq::transport::IPAddress addr, daq::transport::IPPort port);

    // Create NetAddress from sockaddr_storage
    explicit NetAddress(const sockaddr_storage& addr);

    // Copy construct NetAddress
    NetAddress(const NetAddress& other);
    ~NetAddress();

    // Assign NetAddress
    NetAddress& operator=(const NetAddress& other);

public:
    // return port number
    daq::transport::IPPort port() const;

    // return host name
    std::string    hostname() const;

    // return IP address for host name
    daq::transport::IPAddress inet_addr() const;

    // return IPV^ address
    in6_addr inet_addr_v6() const;

    // fill sockaddr with both IP address and port number
    // works for both IPV4 and IPV6
    bool getsockaddr(sockaddr_storage& saddr) const;

    // address family, AF_INET or AF_INET6
    int  family() const;

public:
    // return name of local host (like gethostname(2))
    static std::string self();

private:

    bool resolve() const;

    mutable bool             m_resolved;
    mutable std::string      m_hostname;
    daq::transport::IPPort   m_port;
    mutable sockaddr_storage m_addr;

    friend std::ostream& operator<<(std::ostream&, const NetAddress&);

};

std::ostream& operator<<(std::ostream&, const NetAddress&);

#endif // TRANSPORT_NETADDRESS_H_

