/* this is -*- C++ -*- */
#ifndef TRANSPORT_TCP_H_
#define TRANSPORT_TCP_H_

//
// $Id$
//

#include "transport/Transport.h"
#include "transport/SocketDefs.h"

class NetAddress;

/**
 * Interface to TCP transport protocol.
 *
 * The TCP class provides an interface to the TCP/IP protocol. Both
 * client and server connections can be opened.
 */
class TCP : public Transport {
public:
    /** Create client transport and connect to NetAddress; if 'local_port'
     * is not zero, bind local address to 'local_port'. 
     * 
     * Throws:
     * 
     *  TransportCannotCreate
     *  TransportHostNotFound
     *  TransportCannotBind()
     *  TransportCannotConnect()
     * 
     */
    TCP(const NetAddress& addr, unsigned short local_port = 0);

    /*
     * Same as before, but with at timeout in microseconds.
     *
     * Throws:
     *
     *  TransportCannotCreate
     *  TransportHostNotFound
     *  TransportCannotBind()
     *  TransportCannotConnect()
     */
    TCP(const NetAddress& addr, unsigned short local_port, unsigned int timeout_in_us);

    /** Create server transport connection (listening socket) and bind
     * it to  'local_port'. Call 'accept()' to accept incoming messages.
     * A successfull 'can_receive()' call on this object means that 'accept()'
     * will not block.
     *
     * Throws:
     *
     * TransportCannotCreate
     * TransportHostNotFound
     * TransportCannotBind()
     * 
     */
    explicit TCP(unsigned short local_port);
    ~TCP();
protected:
    TCP(SOCKET socket, unsigned short local_port, const NetAddress& addr);
public:

    using Transport::send;

    /**
     * Special implementation that will handle iovec > UIO_MAXIOV)
     */
    virtual bool send(int n, struct iovec *vec);

    /* Receive exactly n bytes. Returns 'length'.
     * Throws:
     *
     *   TransportCannotReceive
     */
    size_t receive_count(void *data, size_t length);

    /* Receive exactly n bytes. 'timeout' is in microseconds. If the timeout
     * expires, returns the number of bytes read so far.
     * Throws:
     *
     *   TransportCannotReceive
     */
    size_t receive_count(void *data, size_t length, unsigned int timeout);

    /*
     * TCP specific options
     *
     * this includes only what we need for now.
     */

    /** 
     * Enable/disable Nagle algorithm.
     */
    bool set_nodelay(bool on);

    /**
     * Enable/disable non-blocking IO.
     * At the moment this is only possible for listening sockets.
     */
    bool set_nonblocking(bool on);

    /*
     * Is connection non-blocking ?
     */
    bool is_nonblocking() const;

    /**
     * Get underlying SOCKET object.
     */
    SOCKET get_handle() const;
    
public:
    /** Accept an incoming TCP connection. Returns a new TCP object which can
     * be used to send/receive data. Returns NULL on error.
     */
    TCP  *accept();

protected:

    void common_init(int af);

    unsigned short m_port;
    SOCKET         m_socket;
    bool           m_listening;
    bool           m_is_nonblocking;
};

#endif // TRANSPORT_TCP_H_
