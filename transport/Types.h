// this is -*- c++ -*-
#ifndef TRANSPORT_TYPES_H_
#define TRANSPORT_TYPES_H_

namespace daq {
    namespace transport {
        typedef unsigned int IPAddress;
        typedef unsigned short IPPort;
    }
}

#endif // TRANSPORT_TYPES_H_
