// this is -*- c++ -*-
#ifndef TRANSPORT_EPOLLMANAGER_H_
#define TRANSPORT_EPOLLMANAGER_H_

#include "transport/SocketManager.h"

struct epoll_event;

namespace daq {
    namespace transport {

        /**
         * An implementation of the SocketManager interface
         * using the epoll(7) functions.
         * This works only on Linux kernel >= 2.6.x
         */
        
        class EpollManager : public SocketManager {
        public:
            /// Size hint for kernel: approx. no filedescriptors to be used
            explicit EpollManager(int size = 64);
            virtual ~EpollManager();
        public:                         // changing the socket set
            virtual void    add_handle(SOCKET s, Mode mode) override;
            virtual void    remove_handle(SOCKET s) override;
            
        public:                         
            // check if any socket is ready
            virtual bool   ready() override;
            
            // waiting for data, returns SOCKET_ERROR if none available
            virtual SOCKET next() override;
            
            // timeout is in microseconds
            virtual SOCKET next(unsigned int timeout) override;
            
        protected:
            
            void wait_poll(int timeout);
            SOCKET get_next();
            
            // poll structure array
            struct epoll_event  *m_poll;
            
            /// size of m_poll array
            int            m_max_fd;
            
            // number of ready file descriptors
            int            m_num_ready;
            
            SOCKET m_fd;
        };
    }
}

// for backwards compatibility  for a while
using daq::transport::EpollManager;

#endif // TRANSPORT_EPOLLMANAGER_H_
