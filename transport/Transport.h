/* this is -*- C++ -*- */
#ifndef TRANSPORT_TRANSPORT_H_
#define TRANSPORT_TRANSPORT_H_

//
// $Id$
//

#include <stdlib.h>
#include <list>
#include <utility>
#include <exception>

#include "transport/SocketDefs.h"
#include "transport/NetAddress.h"

#include "ers/Issue.h"

ERS_DECLARE_ISSUE(transport,
		  TransportException, ERS_EMPTY, ERS_EMPTY)

//
// exceptions thrown 
//

/// Cannot create socket exception
ERS_DECLARE_ISSUE_BASE(transport,
		       TransportCannotCreate,
		       TransportException,
		       "Cannot create socket:" << m_errno_msg, 
                       ERS_EMPTY, 
		       ((std::string)m_errno_msg)
		       )
		       
/// Cannot find host exception
ERS_DECLARE_ISSUE_BASE(transport,
		       TransportHostNotFound,
		       TransportException,
		       "Cannot find host: " << m_hostname << " reason: " << m_errno_msg,
                       ERS_EMPTY,
		       ((std::string)m_errno_msg)
		       ((std::string)m_hostname)
		       )

/// Cannot send through socket exception
ERS_DECLARE_ISSUE_BASE(transport,
		       TransportCannotSend,
		       TransportException, 
		       "Cannot send: " << m_errno_msg,
		       ERS_EMPTY,
		       ((std::string)m_errno_msg)
		       )

/// Cannot receive from socket exception
ERS_DECLARE_ISSUE_BASE(transport,
		       TransportCannotReceive,
		       TransportException, 
		       "Cannot receive:" << m_errno_msg, 
		       ERS_EMPTY,
		       ((std::string)m_errno_msg)
		       )

/// Cannot bind to address exception
ERS_DECLARE_ISSUE_BASE(transport,
		       TransportCannotBind,
		       TransportException, 
		       "Cannot bind to port " << m_port << " reason: " << m_errno_msg,
		       ERS_EMPTY,
		       ((std::string)m_errno_msg)
		       ((unsigned short)m_port) 
		       )

/// Cannot connect to address exception
ERS_DECLARE_ISSUE_BASE(transport,
		       TransportCannotConnect,
		       TransportException,
		       "Cannot connect to host: " << m_host << " port: " << m_port << " reason: " << m_errno_msg,
                       ERS_EMPTY,
		       ((std::string)m_errno_msg)
  	               ((std::string)m_host)
		       ((unsigned short)m_port) 
		       )

ERS_DECLARE_ISSUE_BASE(transport,
		       TransportNotSupported,
                       TransportException,
                       "Not supported: " << m_errno_msg,
                       ERS_EMPTY,
                       ((std::string)m_errno_msg)
                       )

using namespace transport;

/**
 * Generic Transport protocol interface.
 * Transport provides a generic and platform independent interface
 * to TCP and UDP.
 */
class Transport {
protected:
    Transport(const NetAddress& addr);
public:
    virtual ~Transport();
public:
    // Send data through transport connection
    virtual bool send(const void *data, size_t length);

    // Gather data from list of addresses plus sizes
    virtual bool send(std::list<std::pair<void*,size_t> >& data);

    // alternative scatter/gather interface, use iovec directly
    virtual bool send(int n, struct iovec *vec);
    
    // Blocking receive of data through transport connection.
    virtual size_t receive(void *data, size_t length);

    /* Non-blocking receive. 'timeout' is in microseconds. If 'timeout'
     * is zero, this corresponds to a polling call.
     */
    virtual size_t receive(void *data, size_t length, 
                           unsigned int timeout);
    
    // Scatter receive.
    virtual size_t receive(std::list<std::pair<void *, size_t> >& data);

    // Alternative scatter/gather interface, use iovec directly
    virtual size_t receive(int n, struct iovec *vec);

    // Return true if this connection can send without blocking.
    virtual bool can_send() const;

    // Return true if this connection can receive without blocking.
    virtual bool can_receive() const;

    // return address this connection is attached to.
    const NetAddress& address() const;

    // get underlying SOCKET
    virtual SOCKET get_handle() const = 0;

    // return local port number
    virtual unsigned short local_port();
    
    // set send buffer size
    virtual void set_send_buffer(size_t bufsize);

    // get send buffer size
    virtual size_t get_send_buffer() const;

    // set receive buffer size
    virtual void set_receive_buffer(size_t bufsize);

    // get receive buffer size
    virtual size_t get_receive_buffer() const;

private:
    NetAddress m_addr;
};

#endif // TRANSPORT_TRANSPORT_H_
