// this is -*- c++ -*-
#ifndef TRANSPORT_NETWORK_H_
#define TRANSPORT_NETWORK_H_

#include "transport/Types.h"
#include "ers/Issue.h"

#include <string>
#include <iosfwd>

ERS_DECLARE_ISSUE(transport,
                  NetworkInvalidAddress, ERS_EMPTY, ERS_EMPTY)

namespace daq {
    namespace transport {

        class Network {
        public:
            // Format: A.B.C.D[/Z] or A.B.C.D[/W.X.Y.Z]
            Network(const std::string& nw);
            Network(IPAddress addr, IPAddress mask);
            IPAddress address() const;
            IPAddress netmask() const;
            bool operator==(const Network& other) const;
            bool matches(IPAddress addr) const;
        private:
            IPAddress m_addr;
            IPAddress m_mask;
        };

        std::ostream& operator<<(std::ostream& os, const Network& nw);
    }
}

#endif // TRANSPORT_NETWORK_H_
