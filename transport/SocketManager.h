// this is -*- c++ -*-
#ifndef TRANSPORT_SOCKETMANAGER_H_
#define TRANSPORT_SOCKETMANAGER_H_

#include "transport/SocketDefs.h"

namespace daq {
    namespace transport {

        //
        // Abstract base class for socket managers.
        // This allows to wait for input from multiple
        // sockets and retrieve the next available socket
        // with or without timeout.
        //
        // This is an alternative to the Select class and
        // works directly with the underlying sockets
        // instead of 'Transport' objects.
        //


        class SocketManager {
        public:
            enum Mode { Read, Write, ReadWrite };
            virtual ~SocketManager() {};
        public:                         // changing the socket set
            virtual void    add_handle(SOCKET s, Mode mode = Read)    = 0;
            virtual void    remove_handle(SOCKET s) = 0;

        public:                         
            // check if any socket is ready
            virtual bool   ready()                    = 0;

            // waiting for data, returns SOCKET_ERROR if none available
            virtual SOCKET next()                     = 0;

            // timeout is in microseconds
            virtual SOCKET next(unsigned int timeout) = 0;
        };

    }
}
using daq::transport::SocketManager;

#endif // TRANSPORT_SOCKETMANAGER_H_
