// this is -*- c++ -*-
#ifndef TRANSPORT_SOCKETDEFS_H
#define TRANSPORT_SOCKETDEFS_H

// $Id$

//
// adapt socket definitions for different operating systems
//

//
// this header files defines:
//
// SOCKET    - type to use for a socket
// socklen_t - type to use for sizeof(sockaddr)
// other definitions (e.g. for sys/time.h for select)
// WinsockInit makes sure that WINSOCK2 is initialized
//

#ifdef WIN32

#include <winsock2.h>
#include "os/WinsockInit.h"
static WinsockInit s_winsockinit;

#else 

#define SOCKET int
#include <sys/time.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <netdb.h>
#include <unistd.h>
#include <sys/uio.h>
#include <sys/un.h>

#define SOCKET_ERROR   (-1)
#define INVALID_SOCKET (-1)
inline int closesocket(int s) { return close(s); }

#endif /* WIN32 */

#ifndef __linux__
typedef int socklen_t;
#endif // linux

#endif // TRANSPORT_SOCKETDEFS_H
