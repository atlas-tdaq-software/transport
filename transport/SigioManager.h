// this is -*- c++ -*-
#ifndef TRANSPORT_SIGIOMANAGER_H_
#define TRANSPORT_SIGIOMANAGER_H_

#include <unistd.h>
#include <signal.h>
#include "transport/PollManager.h"

namespace daq {
    namespace transport {

        //
        // An implementation of the SocketManager interface
        // using Posix 1b real-time signals and the 
        // sigwaitinfo()/sigtimedwait() interface.
        // Works only on Unix systems that support those
        // calls. Requires Linux kernel >= 2.4.
        //

        //
        // Note: This object MUST be created by the thread
        //       that later calls ready()/next()
        //
        class SigioManager : public PollManager {

        public:
            // signum must be one of the realtime signals
            // i.e. > SIGRTMIN
            SigioManager(int signum = SIGRTMIN + 1);
            virtual ~SigioManager();

        public:                         // changing the socket set
            virtual void    add_handle(SOCKET s, Mode mode) override;
            virtual void    remove_handle(SOCKET s) override;

        public:                         
            // check if any socket is ready
            virtual bool   ready() override;

            // waiting for data, returns SOCKET_ERROR if none available
            virtual SOCKET next() override;

            // timeout is in microseconds
            virtual SOCKET next(unsigned int timeout) override;

        private:
            SOCKET wait_sig(int timeout);

        private:
            int      m_signal;
            pid_t    m_pid;
            sigset_t m_signal_set;
            SOCKET   m_sig_fd;
        };
    }
}

#endif // TRANSPORT_SIGIOMANAGER_H_
