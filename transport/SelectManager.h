// this is -*- c++ -*-
#ifndef TRANSPORT_SELECTMANAGER_H_
#define TRANSPORT_SELECTMANAGER_H_

#include "transport/SocketManager.h"


namespace daq {
    namespace transport {

        //
        // An implementation of SocketManager
        // using the select() functions.Should
        // work on all Unix systems and even
        // Windows NT.
        //

        class SelectManager : public SocketManager {
        public:
            SelectManager();
            virtual ~SelectManager() {};
        public:                         // changing the socket set
            virtual void    add_handle(SOCKET s, Mode mode) override;
            virtual void    remove_handle(SOCKET s) override;

        public:                         
            // check if any socket is ready
            virtual bool   ready() override;

            // waiting for data, returns SOCKET_ERROR if none available
            virtual SOCKET next() override;

            // timeout is in microseconds
            virtual SOCKET next(unsigned int timeout) override;

        private:
            void   wait_more(struct timeval *timeout);
            SOCKET get_next();

            fd_set m_set;
            fd_set m_write_set;
            SOCKET m_max;

            fd_set m_ready;
            fd_set m_write_ready;
            SOCKET m_num_ready;

        };
    }
}
using daq::transport::SelectManager;

#endif // TRANSPORT_SELECTMANAGER_H_
