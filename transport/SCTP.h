/* this is -*- C++ -*- */
#ifndef TRANSPORT_SCTP_H_
#define TRANSPORT_SCTP_H_

//
// $Id$
//

#include "transport/Transport.h"
#include "transport/SocketDefs.h"

class NetAddress;

/**
 * Interface to SCTP transport protocol.
 *
 * The SCTP class provides an interface to the SCTP protocol. Both
 * client and server connections can be opened.
 */
class SCTP : public Transport {
public:
    /** Create client transport and connect to NetAddress; if 'local_port'
     * is not zero, bind local address to 'local_port'. This constructor
     * only creates SOCK_STREAM type connections.
     *
     * Throws:
     * 
     *  TransportCannotCreate
     *  TransportHostNotFound
     *  TransportCannotBind()
     *  TransportCannotConnect()
     * 
     */
    SCTP(const NetAddress& addr, unsigned short local_port = 0);

    /** Create server transport connection (listening socket) and bind
     * it to  'local_port'. 

     * If 'stream' is true, create a SOCK_STREAM type
     * socket a la TCP. Call 'accept()' to accept incoming messages.
     * A successfull 'can_receive()' call on this object means that 'accept()'
     * will not block.
     *
     * If 'stream' is false, create SOCK_SEQPACK type socket. Use the 
     * SCTP specified send() function to implicitly create connections.
     * 
     * If 'num_streams' is non-zero, negotiate the number of streams to
     * use with the peer. We use the same number for input and output streams.
     *
     * Throws:
     *
     * TransportCannotCreate
     * TransportHostNotFound
     * TransportCannotBind()
     * 
     */
    explicit SCTP(unsigned short local_port, bool stream = true, int num_streams = 0);

    ~SCTP();
private:
    SCTP(SOCKET socket, unsigned short local_port, const NetAddress& addr);
public:

    using Transport::send;
    using Transport::receive;

    /**
     * Send to specified address, works only for SOCK_SEQPACK type
     * connections. Create a new association if necessary.
     *
     * 'stream' can be a value between 0 (default), and the max number
     * that was given in the constructor.
     *
     * If 'unordered' is true, the data is sent as unordered. In this case
     * the receiver should set the OOBINLINE flag via set_oobinline(true) to
     * receive this inside the normal data stream.
     */
    bool send(int n, struct iovec *vec, const NetAddress& addr, int stream = 0, bool unordered = false);

    /**
     * Get underlying SOCKET object.
     */
    SOCKET get_handle() const;

    /**
     * Override receive() method to handle reading of partial
     * messages properly.
     */
    size_t receive(int n, iovec *vec, bool& more);

    /** 
     * Enable/disable Nagle algorithm.
     */
    bool set_nodelay(bool on);

    /**
     * Enable/disable receiving OOB data inline.
     */
    bool set_oobinline(bool on);

    int  num_streams() const;
    
public:
    /** Accept an incoming SCTP connection. Returns a new SCTP object which can
     * be used to send/receive data. Returns NULL on error. This only works
     * for SOCK_STREAM type connections.
     */
    SCTP  *accept();

private:
    unsigned short m_port;
    SOCKET         m_socket;
    bool           m_listening;
    int            m_num_streams;
};

#endif // TRANSPORT_SCTP_H_
